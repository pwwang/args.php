<?php
require_once __DIR__ . "/../vendor/autoload.php";

$args = pw\Args\Args::factory ()
	-> allowNoGap ()
    -> option  ("a")
    -> alias   ("A")
    -> long    ("abc")
    -> desc    ("Option a")
    -> require ()
    -> end     ()
    
    -> option  ("b")
    -> alias   ("B")
    -> long    ("bbc")
    -> desc    ("Option b")
    -> default ("10")
    -> end     ()
    
    -> option  ("c")
    -> long    ("optc")
    -> bool    ()
    -> default (true)
    -> desc    ("Option c, a bool option")
    -> end     ()
    
    -> option  ("d")
    -> long    ("optd")
    -> flag    ()
    -> desc    ("Whether something is done.")
    -> end     ()
    
    -> option  ("e")
    -> array   ()
    -> require ()
    -> desc    ("Option e")
    -> callback(function ($opt) { $opt->setValue (implode(",", $opt->getValue())); })
    -> end     ()
    
    -> option  ()
    -> array   ()
    -> require ()
    -> desc    ("Option f")
    -> end     ()
	
    -> done    ();
// expect arguments: -a1 - 4 5 -ea ab 1 2 - 1 2    
pw\Utils\Utils::dump($args->get());