<?php
require __DIR__ . "/../vendor/autoload.php";

$args = pw\Args\Args::factory()
		->setHelpOpts ('-?, --HELP')
		->option ('a')
		->desc ('An option.')
		->default (1)
		->end();
$args->done ();