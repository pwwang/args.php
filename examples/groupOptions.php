<?php
require __DIR__ . "/../vendor/autoload.php";

$args = pw\Args\Args::factory();
$args->helpOptGroup ('Group1 options');
$args->option ('a')->desc('Option a')->group ('Group1 options');
$args->option ('b')->desc('Option b')->group ('Group1 options');
$args->option ('c')->desc('Option c')->group ('Group2 options');
$args->option ('d')->desc('Option d')->group ('Group2 options');
$args->done ();
pw\Utils\Utils::dump ($args->get());