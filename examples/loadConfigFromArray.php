<?php
require __DIR__ . "/../vendor/autoload.php";

$args = pw\Args\Args::factory()
	-> loadFromArray ([
		"helpOptGroup" => "Group1 options",
		"options" => [
			"a" => [
				"desc" => "Option a",
				"group" => "Group1 options"
			],
			"b" => [
				"desc" => "Option b",
				"group" => "Group1 options"
			],
			"c" => [
				"desc" => "Option c",
				"group" => "Group2 options"
			],
			"d" => [
				"desc" => "Option d",
				"group" => "Group2 options"
			]
		]
	]);
$args-> done ();
	
pw\Utils\Utils::dump ($args->get());