<?php
require __DIR__ . "/../vendor/autoload.php";

$args = pw\Args\Args::factory()
		->helpOnBald()
		->showTypes()
		->option ('a')
		->desc ('An option.')
		->require ()
		->array()
		->end();
$args->done ();