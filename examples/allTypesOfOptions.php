<?php
require_once __DIR__ . "/../vendor/autoload.php";

$args = pw\Args\Args::factory ()
    -> option  ("a")
    -> alias   ("a1")
    -> long    ("abc")
    -> desc    ("Option a")
    -> require ()
    -> end     ()
    
    -> option  ("b")
    -> alias   ("b1")
    -> long    ("bbc")
    -> desc    ("Option b")
    -> default ("10")
    -> end     ()
    
    -> option  ("c")
    -> long    ("optc")
    -> bool    ()
    -> default (true)
    -> desc    ("Option c, a bool option")
    -> end     ()
    
    -> option  ("d")
    -> long    ("optd")
    -> flag    ()
    -> desc    ("Whether something is done.")
    -> end     ()
    
    -> option  ("e")
    -> array   ()
    -> require ()
    -> desc    ("Option e")
    -> callback(function ($opt) { $opt->setValue (implode(",", $opt->getValue())); })
    -> end     ()
    
	-> option  ("v")
	-> incr    (3)
	-> aka     ("vb")
	-> long    ("verbose")
	-> desc    ("Verbose")
	-> default (1)
	-> end     ()
	
    -> option  ()
    -> array   ()
    -> require ()
    -> desc    ("Option f")
    -> end     ()
	
    -> done    ();
// expect arguments: -a 1 - 4 5 -e a ab 1 2 - 1 2 
pw\Utils\Utils::dump($args->get());