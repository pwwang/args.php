<?php
require __DIR__ . "/../vendor/autoload.php";

$args = pw\Args\Args::factory ()
    ->option ("file")
    ->require()
    ->callback (function($opt) {
        $val = $opt->getValue ();
        if (!is_file($val)) return false;
        $opt->setValue (fopen ($val, "r"));
    }, "File does not exist.")
    ->end()
    ->done();

echo fgets($args["file"]) . PHP_EOL;
fclose ($args["file"]);