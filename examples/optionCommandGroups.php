<?php
require_once __DIR__ . "/../vendor/autoload.php";

$args = pw\Args\Args::factory ()
	-> optGroupOrder ('Optional options, Required options')
	-> cmdGroupOrder ('Group1 commands, Group2 commands')
	-> helpOptGroup ('Optional options')
	-> helpCmdGroup ('Group2 commands')
	-> showTypes ()
	
    -> option  ("a")
    -> alias   ("a1")
    -> long    ("abc")
    -> desc    ("Option a")
    -> require ()
	-> group   ("Required options")
    -> end     () // return to args
    
	-> command ('g1-cmd')
	-> desc    ('The command belongs to group1')
	-> group   ('Group1 commands')
	
    -> option  ("b")
    -> alias   ("b1")
    -> long    ("bbc")
    -> desc    ("Option b")
    -> default ("10")
	-> group   ("Optional options")
    -> end     () // return to command (g1-cmd)
	
	-> end     () // return to args
	-> command ('g2-cmd')
	-> desc    ('The command belongs to group2')
	-> group   ('Group2 commands')
    
    -> option  ("c")
    -> long    ("optc")
    -> bool    ()
    -> default (true)
	-> group   ("Optional options")
    -> desc    ("Option c, a bool option")
    -> end     ()

    -> option  ("d")
    -> long    ("optd")
	-> group   ("Optional options")
    -> flag    ()
    -> desc    ("Whether something is done.")
    -> end     ()
    
    -> option  ("e")
    -> array   ()
    -> require ()
	-> group   ("Required options")
    -> desc    ("Option e")
    -> callback(function ($opt) { $opt->setValue (implode(",", $opt->getValue())); })
    -> end     ()
    
    -> option  ()
    -> array   ()
    -> require ()
	-> group   ("Required options")
    -> desc    ("Option f")
    -> end     ()
	
	-> end     () // return to args
	
    -> done    ();
// expect arguments: -a 1 g1-cmd g2-cmd - 4 5 -e a ab 1 2 - 1 2 
pw\Utils\Utils::dump($args->get());