<?php
require __DIR__ . "/../vendor/autoload.php";

$args = pw\Args\Args::factory();
$opt  = $args->option ()
		->array ()
		->desc ('Input files.')
		->require ();
$args->done ();

pw\Utils\Utils::dump($args->get());