<?php
require_once __DIR__ . "/../vendor/autoload.php";

$args = pw\Args\Args::factory ();
$cmd1 = $args -> command ("command1")
    -> desc    ('The first command')
	
    -> option  ("b")
    -> alias   ("B")
    -> long    ("bbc")
    -> desc    ("Option b")
    -> default ("10")
    -> end     ()
    
    -> option  ("c")
    -> long    ("optc")
    -> bool    ()
    -> default (true)
    -> desc    ("Option c, a bool option")
    -> end     ();
$cmd2 = $args -> command ("command2")
	-> aka     ('command-2')
	-> desc    ('The second command')
    -> option  ("d")
    -> long    ("optd")
    -> flag    ()
    -> desc    ("Whether something is done.")
    -> end     ()
    
    -> option  ("e")
    -> array   ()
    -> require ()
    -> desc    ("Option e")
    -> callback(function ($opt) { $opt->setValue (implode(",", $opt->getValue())); })
    -> end     ();
$args -> done();

pw\Utils\Utils::dump($args->get());