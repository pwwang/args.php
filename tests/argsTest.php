<?php
require_once __DIR__ . "/../vendor/autoload.php";
use pw\Args\Args;
use pw\Args\Option;
use pw\Args\Command;

class argsTest extends PHPUnit_Framework_TestCase {

	private function addTail ($str) {

		$array = explode("\n", $str); 
		$array = array_map (function ($v) { return (!empty($v) and $v[strlen($v)-1] == "|") ? $v : $v . "|"; }, $array);
		return implode ("\n", $array);
	}
	
	/**
	 * Call protected/private method of a class.
	 *
	 * @param object &$object    Instantiated object that we will run method on.
	 * @param string $methodName Method name to call
	 * @param array  $parameters Array of parameters to pass into method.
	 *
	 * @return mixed Method return.
	 */
	private function invokeMethod(&$object, $methodName, $parameters = []) {
	   $reflection = new \ReflectionClass(get_class($object));
	   $method = $reflection->getMethod($methodName);
	   $method->setAccessible(true);
	
	   return $method->invokeArgs($object, $parameters);
	}
	
	/**
	 * Get value of protected/private property.
	 *
	 * @param object &$object    Instantiated object that we will run method on.
	 * @param string $property   Property name to get
	 *
	 * @return mixed Property value return.
	 */
	private function invokeProperty(&$object, $property) {
		$reflection = new \ReflectionClass(get_class($object));
		$prop = $reflection->getProperty($property);
		$prop->setAccessible(true);
		return $prop->getValue($object);
	}
	
	public function testInit () {
		$args = new Args();
		
		$this->assertInstanceOf ('pw\Args\Args', $args);
		
		$this->assertEquals ([], $this->invokeProperty($args, 'desc'));
		$this->assertEquals (["help"], $this->invokeProperty($args, 'helpCmds'));
		$this->assertEquals (["-h", "--help"], $this->invokeProperty($args, 'helpOpts'));
		$this->assertEquals ([], $this->invokeProperty($args, 'options'));
		$this->assertEquals ([], $this->invokeProperty($args, 'commands'));
		/*
		|<----------- 40 ------------->| 4 |<-----27----->|
		|  -h, --help                   -> print ...
		|  | 4 |<---------- 34 --------|
		*/
		$this->assertEquals (4, $this->invokeProperty($args, 'maxNameLen'));
		$this->assertEquals (34, $this->invokeProperty($args, 'maxLongLen'));
		$this->assertEquals (27, $this->invokeProperty($args, 'maxDescLen'));
		$this->assertEquals (71, $this->invokeProperty($args, 'maxTotaLen'));
		$this->assertEquals (["{prog} [options]"], $this->invokeProperty($args, 'usage'));
		$this->assertEquals ([], $this->invokeProperty($args, 'values'));
		$this->assertEquals (false, $this->invokeProperty($args, 'baldHelp'));
		$this->assertEquals (true, $this->invokeProperty($args, 'showWarns'));
		$this->assertEquals (false, $this->invokeProperty($args, 'allowNoGap'));
		$this->assertEquals (false, $this->invokeProperty($args, 'done'));
		$this->assertEquals ('', $this->invokeProperty($args, 'command'));
		
	}
	
	/**
	 * @dataProvider testAdjustLenData
	 */
	public function testAdjustLen ($namelen, $longlen, $desclen, $totalen, $namelen1, $longlen1, $desclen1, $totalen1) {
		$args = new Args();
		$args->_adjustLen ($namelen, $longlen, $desclen, $totalen);
		$this->assertEquals ($namelen1, $args->_getLen("name"));
		$this->assertEquals ($longlen1, $args->_getLen("long"));
		$this->assertEquals ($desclen1, $args->_getLen("desc"));
		$this->assertEquals ($totalen1, $args->_getLen("total"));
	}
	
	public function testAdjustLenData () {
		return [
			[1, 0, 0, 0,    4, 34, 27, 71],
			[5, 0, 0, 0,    5, 34, 27, 72],
			[0, 4, 0, 0,    4, 34, 27, 71],
			[0, 50, 0, 0,   4, 50, 27, 87],
			[0, 0, 8, 0,    4, 34, 27, 71],
			[0, 0, 38, 0,   4, 34, 38, 82],
			[0, 0, 0, 50,   4, 34, 27, 71],
			[0, 0, 0, 100,  4, 34, 56, 100],
		];
	}
	
	public function testCommandIsMe () {
		$args = Args::factory ();
		$args->command ("command1")
			 ->aka ("command-one");
			 
		$this->assertTrue ($args->getCommand("command1")->isMe("command1"));
		$this->assertTrue ($args->getCommand("command-one")->isMe("command1"));
		$this->assertTrue ($args->getCommand("command1")->isMe("command-one"));
		$this->assertFalse ($args->getCommand("command1")->isMe("command-one1"));
	}
	
	public function testCheckCmdName () {
		$args = Args::factory ();
		$args->command ("command1")
			 ->aka ("command-one");
		
		$check1 = $args->_checkCmdName ("command2");
		$check2 = $args->_checkCmdName ("command1");
		$check3 = $args->_checkCmdName ("command-one");
		$check4 = $args->_checkCmdName ("");
		
		$this->assertEquals (true, $check1);
		$this->assertEquals ("Command command1 has been taken by command1.", $check2);
		$this->assertEquals ("Command command-one has been taken by command1.", $check3);
		$this->assertEquals ("Command name cannot be empty.", $check4);
		$this->assertEquals ("Command name should not start with '-', will be confused with options.", $args->_checkCmdName ("-a"));
	}
	
	public function testOptionIsMe () {
		$args = Args::factory ();
		$option = $args->option ("o")
			 ->aka("option1")
			 ->long ("option-one")
			 ->aka ("option-1");
		
		$this->assertEquals (true, $option->isMe("-option1"));
		$this->assertEquals (true, $option->isMe("-o"));
		$this->assertEquals (true, $option->isMe("--option-one"));
		$this->assertEquals (true, $option->isMe("-option-1"));
		$this->assertEquals (false, $option->isMe("-option1abc"));
		$this->assertEquals ("=abc", $option->isMe("-option1=abc"));
		$this->assertEquals ("='abc'", $option->isMe("-option1='abc'"));
		$this->assertEquals ("=\"abc\"", $option->isMe("-option1=\"abc\""));
		$this->assertEquals (false, $option->isMe("-oabc"));
		$this->assertEquals ("=abc", $option->isMe("-o=abc"));
		$this->assertEquals ("='abc'", $option->isMe("-o='abc'"));
		$this->assertEquals ("=\"abc\"", $option->isMe("-o=\"abc\""));
		$this->assertEquals (false, $option->isMe("--option-oneabc"));
		$this->assertEquals ("=abc", $option->isMe("--option-one=abc"));
		$this->assertEquals ("='abc'", $option->isMe("--option-one='abc'"));
		$this->assertEquals ("=\"abc\"", $option->isMe("--option-one=\"abc\""));
		$this->assertEquals (false, $option->isMe("-option-1abc"));
		$this->assertEquals ("=abc", $option->isMe("-option-1=abc"));
		$this->assertEquals ("='abc'", $option->isMe("-option-1='abc'"));
		$this->assertEquals ("=\"abc\"", $option->isMe("-option-1=\"abc\""));
		$this->assertEquals (false, $option->isMe("-a"));
		$this->assertEquals (false, $option->isMe("--option1"));
		$this->assertEquals (false, $option->isMe("--o"));
		$this->assertEquals (false, $option->isMe("-option-one"));
		$this->assertEquals (false, $option->isMe("--option-1"));
		$args->allowNoGap();
		$this->assertEquals ("ption1abc", $option->isMe("-option1abc"));
		$this->assertEquals ("abc", $option->isMe("-oabc"));
		$this->assertEquals ("'abc'", $option->isMe("-o'abc'"));
		$this->assertEquals ("\"abc\"", $option->isMe("-o\"abc\""));
		$this->assertEquals ("abc", $option->isMe("-oabc"));
		$this->assertEquals ("abc", $option->isMe("--option-oneabc"));
		$this->assertEquals ("ption-1abc", $option->isMe("-option-1abc"));
		
		$this->assertEquals ("1", $option->isMe("-o1"));
		$this->assertEquals ("0", $option->isMe("-o0"));
		
		$option = $args->option();
		$this->assertEquals (true, $option->isMe("-"));
		$this->assertEquals (false, $option->isMe("-a"));
		$this->assertEquals (false, $option->isMe("--a"));
		$this->assertEquals (false, $option->isMe(""));
		
		$option = $args->option("b")->flag();
		$this->assertEquals (false, $option->isMe("-b1"));
		
		$option = $args->option  ("v")
			-> incr    (3)
			-> aka     ("vb")
			-> long    ("verbose")
			-> desc    ("Verbose")
			-> default (1);
		$this->assertEquals (true, $option->isMe("-v"));
		$this->assertEquals (true, $option->isMe("-vv"));
		$this->assertEquals (true, $option->isMe("-vvv"));
		$this->assertEquals (false, $option->isMe("-vvvv"));
		$this->assertEquals (true, $option->isMe("-vb"));
		$this->assertEquals (true, $option->isMe("--verbose"));
	}
	
	public function testOptionPartOfMe () {
		$args = Args::factory ();
		$option = $args->option ("option1")
			 ->long ("option-one")
			 ->aka ("option-1");
		$this->assertEquals (true, $option->partOfMe("option-", false));
		$this->assertEquals (true, $option->partOfMe("option-", true));
		$this->assertEquals (true, $option->partOfMe("option1", false));
		$this->assertEquals (true, $option->partOfMe("option-o", true));
		$this->assertEquals (false, $option->partOfMe("abc", false));
		$this->assertEquals (false, $option->partOfMe("abc", true));
		$this->assertEquals (false, $option->partOfMe("option-on", false));
		$this->assertEquals (false, $option->partOfMe("option111", true));
		$this->assertEquals (false, $option->partOfMe("", false));
		$this->assertEquals (false, $option->partOfMe("", true));
		$this->assertEquals (false, $option->partOfMe("option-1", true));
		
		$option = $args->option();
		$this->assertEquals (false, $option->partOfMe(""));
		$this->assertEquals (false, $option->partOfMe("option"));
	}
	
	public function testCheckOptName () {
		$args = Args::factory ();
		$args->option ("option1")
			 ->long ("option-one")
			 ->aka ("option-1")
			 ->end()
			 ->option ("option2")
			 ->long ("option-two")
			 ->end();
		
		$this->assertEquals ("Option name 'option1' has been taken by option -option1.", $args->_checkOptName ("option1"));
		$this->assertEquals ("Option long name 'option-one' has been taken by option --option-one(-option1).", $args->_checkOptName ("option-one"));
		$this->assertEquals ("Option name 'option-1' has been taken by option -option1.", $args->_checkOptName ("option-1"));
		$this->assertEquals ("Option name 'option2' has been taken by option -option2.", $args->_checkOptName ("option2"));
		$this->assertEquals ("Option long name 'option-two' has been taken by option --option-two(-option2).", $args->_checkOptName ("option-two"));
		$this->assertEquals ("Option name cannot start with '-'. If you want to define long name, use 'long'.", $args->_checkOptName("-a"));
		$this->assertEquals (true, $args->_checkOptName(""));
		$args->option();
		$this->assertEquals ("Option name '' has been taken by option -.", $args->_checkOptName(""));
		$args->allowNoGap (false);
		$this->assertEquals (true, $args->_checkOptName("opt"));
		$args->allowNoGap (true);
		$this->assertEquals (true, $args->_getAllowNoGap());
		$this->assertEquals ("Option '-opt' is part of option -option1's names. " .
							 "This will be confused for '-a1 -ab1': a=1,ab=1 or a=1,a=b1. " .
							 "To avoid this, set allowNoGap(false), or use a different option name.", $args->_checkOptName("opt"));
		$this->assertEquals ("Option '-option-123' starts with option -option1's names. " .
							 "This will be confused for '-a1 -ab1': a=1,ab=1 or a=1,a=b1. " .
							 "To avoid this, set allowNoGap(false), or use a different option name.", $args->_checkOptName("option-123"));
		$this->assertEquals ("Option '--opt' is part of option -option1's long names. " .
							 "This will be confused for '--a1 --ab1': a=1,ab=1 or a=1,a=b1. " .
							 "To avoid this, set allowNoGap(false), or use a different long name.", $args->_checkOptName("opt", true));
		$this->assertEquals ("Option '--option-two2' starts with option -option2's long names. " .
							 "This will be confused for '--a1 --ab1': a=1,ab=1 or a=1,a=b1. " .
							 "To avoid this, set allowNoGap(false), or use a different long name.", $args->_checkOptName("option-two2", true));
		$this->assertEquals (true, $args->_checkOptName("option-123", true));
	}
	
	
	/**
     * @expectedException Exception
     */
	public function testOptionUndefinedMemberFunctionsArgs () {
		$args = Args::factory();
		$args->option()->awfwfefafawfaw();
	}
	

	public function testOptionInit () {
		$args = Args::factory();
		$option = $args->option ("a");
		
		$this->assertEquals ("a", $option->getName());
		$this->assertEquals ("string", $option->getType());
		$this->assertEquals ([], $this->invokeProperty($args, "errors"));
		$this->assertEquals ([], $this->invokeProperty($args, "warnings"));
		$this->assertEquals (["a"], $option->getShorts());
		$this->assertEquals ([], $option->getLongs());
		$this->assertEquals ([], $option->getDesc());
		$this->assertEquals (false, $option->getRequired());
		$this->assertNull ($option->getDefault());
		$this->assertNull ($option->getValue());
		$this->assertNull ($option->getRule());
		$this->assertNull ($option->getRuleError());
		$this->assertNull ($option->getMatched());
	}
	
	public function testUsage () {
		$args = Args::factory ();
		$args->usage ("{prog} -opt1 val1 -opt2 val2 val3 ...");
		
		$this->assertEquals (
			PHP_EOL . pw\Utils\Utils::color("Usage:", "yellow") . PHP_EOL .
			"  " . pw\Utils\Utils::color("phpunit", "bold") . " -opt1 val1 -opt2 val2 val3 ..." . PHP_EOL,
			$this->invokeMethod ($args, "getHelpUsage", ["phpunit"])
		);
		
		$args = Args::factory ();
		$args->usage ("{prog} -opt1 val1 -opt2 val2 val3 ...
					   __{prog} the other usage");
		$this->assertEquals (
			PHP_EOL . pw\Utils\Utils::color("Usage:", "yellow") . PHP_EOL .
			"  " . pw\Utils\Utils::color("phpunit", "bold") . " -opt1 val1 -opt2 val2 val3 ..." . PHP_EOL .
			"    " . pw\Utils\Utils::color("phpunit", "bold") . " the other usage" . PHP_EOL,
			$this->invokeMethod ($args, "getHelpUsage", ["phpunit"])
		);
	}
	
	public function testDesc () {
		$args = Args::factory ();
		$args->desc ("What does this program do?
					  __The other desc of this program.");
		
		$this->assertEquals (
			PHP_EOL . pw\Utils\Utils::color("Description:", "yellow") . PHP_EOL .
			"  What does this program do?" . PHP_EOL . 
			"    The other desc of this program." . PHP_EOL,
			$this->invokeMethod ($args, "getHelpDesc")
		);
		
		// test a long long one
		$longdesc = "This is a very long long description about this programe, I just forgot the break the line. Now check whether the alignment is right for help";
		$onamelen = $args->_getLen ("name");
		$olonglen = $args->_getLen ("long");
		$odesclen = $args->_getLen ("desc");
		$ototalen = $args->_getLen ("total");
		$args->desc ($longdesc);
		$this->assertEquals (
			PHP_EOL . pw\Utils\Utils::color("Description:", "yellow") . PHP_EOL .
			"  $longdesc" . PHP_EOL,
			$this->invokeMethod ($args, "getHelpDesc")
		);
		
		$desclen = strlen($longdesc);
		$this->assertEquals (141, $desclen);
		$this->assertEquals ($onamelen, $args->_getLen("name"));
		$this->assertEquals ($olonglen, $args->_getLen("long"));
		$this->assertEquals ($desclen - 2 - $onamelen - $olonglen - 4, $args->_getLen("desc"));
		$this->assertEquals ($desclen, $args->_getLen("total"));
	}
	
	public function testFlags () {
		$args = Args::factory ();
		$onamelen = $args->_getLen ("name");
		$olonglen = $args->_getLen ("long");
		$args->setHelpOpts ("-?,-H");
		
		$this->assertEquals (["-?", "-H"], $this->invokeProperty($args, "helpOpts"));
		$this->assertEquals ($onamelen, $args->_getLen("name"));
		$this->assertEquals ($olonglen, $args->_getLen("long"));
		$this->assertEquals (true, $this->invokeProperty($args, "helpOptsGlobal"));
		$cmd1 = $args->command ("command");
		$this->assertEquals (["-?", "-H"], $this->invokeProperty($cmd1, "helpOpts"));
		
		$args->setHelpOpts (["-help", "--showhelp"], false);
		$cmd2 = $args->command ("command2");
		$this->assertEquals (["-help", "--showhelp"], $this->invokeProperty($args, "helpOpts"));
		$this->assertEquals (7, $args->_getLen("name"));
		$this->assertEquals ($olonglen, $args->_getLen("long"));
		$this->assertEquals (["-h", "--help"], $this->invokeProperty($cmd2, "helpOpts"));
		
		$args->setHelpCmds ("help,helpcommand");
		$cmd3 = $args->command ("command3");
		$this->assertEquals ($olonglen, $args->_getLen("long"));
		$this->assertEquals (["help","helpcommand"], $this->invokeProperty($cmd3, "helpCmds"));
		
		$args->setHelpCmds ("help,helpcommand,thehelpcommand,thehelpcommand2", false); // strlen = 47
		$cmd4 = $args->command ("command4");
		$this->assertEquals (["help","helpcommand","thehelpcommand","thehelpcommand2"], $this->invokeProperty($args, "helpCmds"));
		$this->assertEquals (7, $args->_getLen("name"));
		$this->assertEquals (strlen("help,helpcommand,thehelpcommand,thehelpcommand2") - 7, $args->_getLen("long"));
		$this->assertEquals (["help"], $this->invokeProperty($cmd4, "helpCmds"));
		
		$this->assertEquals (false, $this->invokeProperty($args, "allowNoGap"));
		$this->assertEquals (false, $this->invokeProperty($args, "baldHelp"));
		$this->assertEquals (true, $this->invokeProperty($args, "showWarns"));
		
		$args->allowNoGap (true);
		$args->helpOnBald(true);
		$args->showWarnings(false);
		$this->assertEquals (true, $this->invokeProperty($args, "allowNoGap"));
		$this->assertEquals (true, $this->invokeProperty($args, "baldHelp"));
		$this->assertEquals (false, $this->invokeProperty($args, "showWarns"));
		$cmd5 = $args->command("command5");
		$this->assertEquals (true, $this->invokeProperty($cmd5, "allowNoGap"));
		$this->assertEquals (true, $this->invokeProperty($cmd5, "baldHelp"));
		$this->assertEquals (false, $this->invokeProperty($cmd5, "showWarns"));
		$args->allowNoGap (true, false);
		$args->helpOnBald(true, false);
		$args->showWarnings(false, false);
		$cmd6 = $args->command("command6");
		$this->assertEquals (false, $this->invokeProperty($cmd6, "allowNoGap"));
		$this->assertEquals (false, $this->invokeProperty($cmd6, "baldHelp"));
		$this->assertEquals (true, $this->invokeProperty($cmd6, "showWarns"));
	}
	/**
	 * @expectedException Exception
	 */ 
	public function testSetHelpOptsException () {
		$args = Args::factory()
			-> setHelpOpts ("--h,--b");
	}
	
	public function testOptionIs () {
		$args = Args::factory ();
		$option = $args->option("a");
		$this->assertTrue ($option->is($option));
	}
	
	
	/**
	 * @expectedException Exception
	 */
	public function testOptionTypes () {
		$args = Args::factory ();
		$option = $args->option("a");
		$this->assertEquals ("string", $option->getType());
		$option->array();
		$this->assertEquals ("array", $option->getType());
		$option->bool();
		$this->assertEquals ("bool", $option->getType());
		$option->json();
		$this->assertEquals ("json", $option->getType());
		$option->incr();
		$this->assertEquals ("incr", $option->getType());
		$option->int();
		$this->assertEquals ("int", $option->getType());
		$option->float();
		$this->assertEquals ("float", $option->getType());
		$option->flag();
		$this->assertEquals ("flag", $option->getType());
		$option->type ("/^\d+$/");
		$this->assertEquals ("/^\d+$/", $option->getType());
		$option->type ("/^\d+$"); // exception raised
		$this->assertEquals ("/^\d+$", $option->getType());
	}
	
	public function testOptionDesc () {
		$args = Args::factory ();
		$option = $args->option ("a")
					   ->desc ("What does this option mean?
								__T_he other desc of this option.");
		
		$this->assertEquals (
			["What does this option mean?", "  T_he other desc of this option."],
			$this->invokeProperty ($option, "desc")
		);
		
		// test a long long one
		$longdesc = "This is a very long long description about this programe, I just forgot the break the line. Now check whether the alignment is right for help";
		$onamelen = $args->_getLen ("name");
		$olonglen = $args->_getLen ("long");
		$odesclen = $args->_getLen ("desc");
		$ototalen = $args->_getLen ("total");
		$option->desc ($longdesc);
		$this->assertEquals (
			[$longdesc],
			$this->invokeProperty ($option, "desc")
		);
		
		$desclen = strlen($longdesc);
		$this->assertEquals (141, $desclen);
		$this->assertEquals ($onamelen, $args->_getLen("name"));
		$this->assertEquals ($olonglen, $args->_getLen("long"));
		$this->assertEquals ($desclen, $args->_getLen("desc"));
		$this->assertEquals ($desclen + 6 + $onamelen + $olonglen, $args->_getLen("total"));
	}
	
	public function testOptionRequire () {
		$args = Args::factory ();
		$option = $args->option ("a");
		$this->assertFalse ($option->getRequired());
		$option->require();
		$this->assertTrue ($option->getRequired());
	}
	
	public function testOptionDefault () {
		$args = Args::factory ();
		$option = $args->option ("a");
		$option->default ("a", "abc");
		$this->assertEquals ("a", $option->getDefault());
		$this->assertEquals ("abc", $option->getDefaultStr());
		
		$option->array();
		$option->default([1,2,3]);
		$this->assertEquals ([1,2,3], $option->getDefault());
		$this->assertEquals ("[1,2,3]", $option->getDefaultStr());
		
		$option->bool()->default("F");
		$this->assertEquals (false, $option->getDefault());
		$this->assertEquals ('false', $option->getDefaultStr());
		
	}
	
	/**
	 * @expectedException Exception
	 */
	public function testOptionDefaultException () {
		$args = Args::factory ();
		$option = $args->option ("a");
		
		$option->type("json");
		$option->default ("{", "optiondefault");
		
		$option->type("flag");
		$option->default("");
	}
	
	public function testOptionAkaLong () {
		$args = Args::factory ();
		
		$onamelen = $args->_getLen ("name");
		$olonglen = $args->_getLen ("long");
		$odesclen = $args->_getLen ("desc");
		$ototalen = $args->_getLen ("total");
		
		$option = $args->option ("a");
		$option->aka("")->aka("abc")->aka("aaa")
			   ->long("ccc")->long("ddddddddddddddddddddddddddddddddddddddda42");
		
		$this->assertEquals (["a", "", "abc", "aaa"], $option->getShorts());
		$this->assertEquals (["ccc", "ddddddddddddddddddddddddddddddddddddddda42"], $option->getLongs());
		
		$this->assertEquals (4, $args->_getLen("name"));
		$this->assertEquals (44, $args->_getLen("long")); // --ddddddddddd...
		$this->assertEquals ($odesclen, $args->_getLen("desc"));
		// 6 == 2 starting spaces  + " -> "
		$this->assertEquals (48+6+$odesclen, $args->_getLen("total"));
	}
	
	/**
	 * @expectedException Exception
	 */
	public function testOptionAkaLongException () {
		$args = Args::factory ();		
		$option = $args->option ("a");
		$option->aka("")->aka("abc")->aka("aaa")
			   ->long("ccc")->long("abc")->aka("a")->long("abcd");
	}
	
	/**
	 * @expectedException Exception
	 */
	public function testOptionAkaLongException1 () {
		$args = Args::factory ();		
		$option = $args->option ("a");
		$option->aka("")->aka("abc")->aka("aaa")
			   ->long("ccc")->long("a");
		$args->allowNoGap();
		$args->option("c");
	}
	
	/**
	 * @expectedException Exception
	 */
	public function testOptionAkaLongException2 () {
		$args = Args::factory ();		
		$option = $args->option ("a");
		$option->aka("")->aka("abc")->aka("aaa")
			   ->long("ccc")->long("a");
		$args->allowNoGap();
		$args->option("abcd");
	}
	
	public function testOptionObey () {
		$args = Args::factory ();		
		$option = $args->option ("a")->obey (function(){}, "{option} error");
		$this->assertTrue (is_callable($option->getRule()));
		$this->assertEquals ("-a error", $option->getRuleError());
	}
	
	public function testOptionSetValue () {
		// default total len = 71 + 4
		$warning = pw\Utils\Utils::color("Warning: Repeated non-array option -a, previous value will be overriden.  ", "bg_yellow", "black") . PHP_EOL;
		$args = Args::factory ();		
		$option = $args->option ("a");
		$option->setValue ("a");
		$this->assertEquals ("a", $option->getValue());
		$this->assertFalse (in_array($warning, $this->invokeProperty($args, "warnings")));
		$option->setValue ("b", true, true);
		$this->assertEquals ("b", $option->getValue());
		$this->assertTrue (in_array($warning, $this->invokeProperty($args, "warnings")));
		
		$option->setValue (null); // clear prev values
		$option->array();
		$option->setValue ([1]);
		$option->setValue ([2,3], false);
		$this->assertEquals ([1,2,3], $option->getValue());
	}
	
	public function testOptionDumpValue () {
		$args = Args::factory ();		
		$option = $args->option ("a");
		$option->aka("")->aka("abc")->aka("aaa")
			   ->long("ccc");
		$option->setValue ("123");
		$option->dumpValue ($values);
		$this->assertEquals ("123", $values[""]);
		$this->assertEquals ("123", $values["abc"]);
		$this->assertEquals ("123", $values["aaa"]);
		$this->assertEquals ("123", $values["ccc"]);
	}
	
	
	public function testOptionGetHelpInfo () {
		$args = Args::factory ();		
		$option = $args->option ("a");
		$option->aka("")->aka("abc")->aka("aaa")
			   ->long("ccc");
		$expect = "  " . pw\Utils\Utils::color("-a", "bold") . ", -                                  -> 
    , -abc                                  
    , -aaa                                  
    , --ccc                                 \n";
		$expect = $this->addTail ($expect);
		$info   = $this->addTail ($option->getHelpInfo());
		//Kint::dump($expect);
		//Kint::dump($info);
		$this->assertEquals ($expect, $info);
		
		$option->default("some value");
		$expect = "  " . pw\Utils\Utils::color("-a, ", "bold") . "-                                  -> ". pw\Utils\Utils::color("Default: \"some value\"", "green") ."
    , -abc                                  
    , -aaa                                  
    , --ccc                                 \n";
		$this->assertEquals ($expect, $option->getHelpInfo());
		
		$option->req ();
		$expect = "  " . pw\Utils\Utils::color("-a, ", "bold") . "-                                  -> ". pw\Utils\Utils::color("Required.", "light_red") ."
    , -abc                                  
    , -aaa                                  
    , --ccc                                 \n";
		$this->assertEquals ($expect, $option->getHelpInfo());
	}
	
	public function testCommandInit () {
		$args = Args::factory ();
		$cmd  = $args->command("cmd");
		$this->assertInstanceOf ('\pw\Args\Command', $cmd);
		
		$this->assertEquals ("cmd", $this->invokeProperty($cmd, "name"));
		$this->assertEquals ("cmd", $cmd->getName());
		$this->assertEquals ([], $this->invokeProperty($cmd, "alias"));
		$this->assertInstanceOf ('\pw\Args\Args', $this->invokeProperty($cmd, "args"));
		$this->assertInstanceOf ('\pw\Args\Args', $cmd->end());
		
	}

	/**
	 * @expectedException Exception
	 */
	public function testCommandInitException () {
		$args = Args::factory ();
		$cmd  = $args->command("cmd");
		$args->command("cmd");
		
	}
	
	public function testCommandGetHelpInfo () {
		$args = Args::factory ();
		$cmd  = $args->command("cmd")
			-> aka ("cmd1")
			-> aka ("cmdddddddddddddddddddddddddddddddddddddddd")
			-> desc ("abc\n    __sdfwe");
		$info = $cmd->getHelpInfo ();
		$this->assertEquals (
			"  " . pw\Utils\Utils::color ("cmd", "bold") . "|cmd1|cmdddddddddddddddddddddddddddddddddddddddd   -> abc\n" .
			"                                                             sdfwe\n",
			$info
		);
	}
	
	/**
	 * @dataProvider testHelpertrimOptValueData
	 */
	public function testHelperTrimOptValue ($x, $y) {
		$this->assertEquals ($y, pw\Args\Helper::trimOptValue ($x));
	}
	
	public function testHelpertrimOptValueData () {
		return [
			['="abc"', "abc"],
			['"abc"', "abc"],
			['\'abc\'', "abc"],
			['=\'abc\'', "abc"],
			['=abc"', "abc\""],
			['="abc', "\"abc"],
			['"abc', "\"abc"],
			['\'abc"', "'abc\""],
		];
	}
	
	/**
	 * @dataProvider testHelperGetArgvData
	 */
	public function testHelperGetArgv ($x, $y) {
		$this->assertEquals ($y, pw\Args\Helper::getArgv ($x));
	}
	
	public function testHelperGetArgvData () {
		return [
			['abc -a 1 -b2 3', ['abc', '-a', '1', '-b2', '3']],
			['"abc" -a "1" -b2 3', ['abc', '-a', '1', '-b2', '3']],
			['"a\\\\\\"bc" -a "1" -b2 3', ['a\\\\\\"bc', '-a', '1', '-b2', '3']],
			['\'a\\\'bc\' -a "1" -b2 3', ['a\\\'bc', '-a', '1', '-b2', '3']],
		];
	}
	
	public function testOptionParse () {
		$args = Args::factory ();
		$option = $args->option ("a");
		
		$this->invokeMethod ($option, 'parse', [['-a', 'abc']]);
		$this->assertEquals ("abc", $option->getValue());
		
		$option->setValue (null);
		$this->invokeMethod ($option, 'parse', [['-a']]);
		$this->assertRegexp ("/No value set for option -a/", implode("", $this->invokeProperty($args, "warnings")));
		
		$args = Args::factory ();
		$args->allowNoGap ();
		$option = $args->option ("a");
		$this->invokeMethod ($option, 'parse', [['-a"123"']]);
		$this->assertEquals ("123", $option->getValue());
		$this->invokeMethod ($option, 'parse', [['-a="123"']]);
		$this->assertEquals ("123", $option->getValue());
		$this->invokeMethod ($option, 'parse', [['-a123']]);
		$this->assertEquals ("123", $option->getValue());
		
		$option->string ("/^\d+$/");
		$this->invokeMethod ($option, 'parse', [['-a"a"']]);
		$this->assertRegexp ("/Invalid value/", implode("", $this->invokeProperty($args, "errors")));
		
		$option->setValue (null, true, false);
		$option->array();
		$this->invokeMethod ($option, 'parse', [['-a"123"', "345", "6"]]);
		$this->assertEquals (["123", "345", "6"], $option->getValue());
		
		$option->bool();
		$option->setValue (null);
		$this->invokeMethod ($option, 'parse', [['-a"F"']]);
		$this->assertEquals (false, $option->getValue());
		$this->invokeMethod ($option, 'parse', [['-ax']]);
		$this->assertEquals (true, $option->getValue());
		$this->invokeMethod ($option, 'parse', [['-a0']]);
		$this->assertEquals (false, $option->getValue());
		
		$option->flag ();
		
		$this->invokeMethod ($option, 'parse', [['-a']]);
		$this->assertEquals (true, $option->getValue());
		
		$option = $args -> option ();
		$option->array();
		$this->invokeMethod ($option, 'parse', [['-', '"123"', "345", "6"]]);
		$this->invokeMethod ($option, 'parse', [['-', '7']]);
		$this->assertEquals (["\"123\"", "345", "6", "7"], $option->getValue());
	}
	
	public function testOptionDone () {
		$args = Args::factory ();
		$option = $args->option ("a");
		
		$option -> callback (function ($opt) {
			$value = $opt->getValue ();
			$opt->setValue ("$value$value");
		});
		
		$option->done (["-a", "abc"]);
		$this->assertEquals ("abcabc", $option->getValue());
		
		$args = Args::factory ();
		$option = $args->option ("a");
		$option -> must (function ($opt) {
			return $opt->getValue()=="111";
		}, "Error happens");
		$option->done (["-a", "abc"]);
		$this -> assertRegexp ("/Error happens/", implode("", $this->invokeProperty($args, "errors")));
		
		$option = $args->option ();
		
	}
	public function testAddHelpOpts () {
		$args1 = pw\Args\Args::factory ();  
		$this->assertEquals (["-h", "--help"], $this->invokeProperty($args1, "helpOpts"));
		$this->invokeMethod ($args1, "addHelpOpts");
		//Kint::dump ($this->invokeProperty($args1, 'options'));
		$opt = $args1->getOption('h');
		$this->assertInstanceOf ('\pw\Args\Option', $opt);
	}
	
	public function testGetHelpOpt () {
		$args1 = pw\Args\Args::factory ();
		$this->assertEquals (["-h", "--help"], $this->invokeProperty($args1, "helpOpts"));   
		$this->invokeMethod ($args1, "addHelpOpts");
		$helpopt = $this->invokeMethod ($args1, "getHelpOpt");
		$this->assertInstanceOf ('\pw\Args\Option', $helpopt);
	}
	
	public function testParseWithoutCommand () {
		/*$args1= pw\Args\Args::factory ()
			-> option  ("a")
			-> alias   ("a1")
			-> long    ("abc")
			-> desc    ("Option a")
			-> require ()
			-> end     ()
			
			-> option  ("b")
			-> alias   ("b1")
			-> long    ("bbc")
			-> desc    ("Option b")
			-> default ("10")
			-> end     ()
			
			-> option  ("c")
			-> long    ("optc")
			-> bool    ()
			-> default (true)
			-> desc    ("Option c, a bool option")
			-> end     ()
			
			-> option  ("d")
			-> long    ("optd")
			-> flag    ()
			-> desc    ("Whether something is done.")
			-> end     ()
			
			-> option  ("e")
			-> array   ()
			-> require ()
			-> desc    ("Option e")
			-> callback(function ($opt) { $opt->setValue (implode(",", $opt->getValue())); })
			-> end     ()
			
			-> option  ()
			-> array   ()
			-> require ()
			-> desc    ("Option f")
			-> end     ();
			
		$this->invokeMethod ($args1, "addHelpOpts");
		$this->invokeMethod ($args1, "parseWithoutCommand", [["-a", "1", "-x", "d", "-", "2", "--optc", "f", "-b", "3", "4", "5"]]);
		
		$optb = $this->invokeMethod ($args1, "getHelpOpt");
		$this->assertRegexp ("/Unknown option\(s\)\/value\(s\): -x, d(?:,|$|\s)/", implode(",", $optb->getWarnings()));
		$opt  = $args1->getOption ("");
		$this->assertEquals (['2', '4', '5'], $opt->getValue());*/
		
		$args2= pw\Args\Args::factory ()
			-> option  ("a")
			-> alias   ("a1")
			-> long    ("abc")
			-> desc    ("Option a")
			-> require ()
			-> end     ()
			
			-> option  ("b")
			-> alias   ("b1")
			-> long    ("bbc")
			-> desc    ("Option b")
			-> default ("10")
			-> end     ()
			
			-> option  ("c")
			-> long    ("optc")
			-> bool    ()
			-> default (true)
			-> desc    ("Option c, a bool option")
			-> end     ()
			
			-> option  ("d")
			-> long    ("optd")
			-> flag    ()
			-> desc    ("Whether something is done.")
			-> end     ()
			
			-> option  ("e")
			-> array   ()
			-> require ()
			-> desc    ("Option e")
			-> callback(function ($opt) { $opt->setValue (implode(",", $opt->getValue()), true); })
			-> end     ()
			
			-> option  ()
			-> array   ()
			-> require ()
			-> desc    ("Option f")
			-> end     ();
			
		$this->invokeMethod ($args2, "addHelpOpts");
		$this->invokeMethod ($args2, "parseWithoutCommand", [explode(" ", "-a 1 - 4 5 -e a ab 1 2 - 1 2")]);
		//Kint::dump ($args2);
		$this->assertEquals ("1", $args2->getOption("a")->getValue ());
		$this->assertEquals ("a,ab,1,2", $args2->getOption("e")->getValue ());
		$this->assertEquals ([4,5,1,2], $args2->getOption("")->getValue());
	}
	
	/**
	 * @dataProvider testThemData
	 */
    public function t1estThem($index, $args, $strargs, $output) {

		$argsout = shell_exec ("php " . __FILE__ . " $index $strargs 2>&1");
		$output  = is_array($output) ? json_encode($output, JSON_PRETTY_PRINT) : $output;
		
		$output  = $this->addTail ($output);
		$argsout = $this->addTail ($argsout);
		
		$this->assertEquals (
			$output,
			$argsout
		);
		
    }
	
	
	public function t1estThemData () {

		$args1 = pw\Args\Args::factory ()
			->opt("a")   ->aka("aaa") ->long("abc") ->req()          ->desc   ("abcabc abcabc\nabcabc") ->end()
			->opt("len") ->long("length")           ->default("10")                                     ->end()
			->opt("f")   ->bool()                   ->default(false) ->desc("Flag\nwhatever")           ->end()
			->opt()      ->array()                  ->require()      ->desc("data files")
				->obey(function($vals){ return sizeof($vals) >= 2; }, "Require at least two data files")->end();
			
		$args2 = pw\Args\Args::factory ();
		$args3 = pw\Args\Args::factory ();
		$args4 = pw\Args\Args::factory ();
		$args5 = pw\Args\Args::factory ();
		
		$ret = [
			[
				$args1,
				"--help",
				"
Usage:
  argsTest.php [options]

Options:
  -a,   -aaa                               -> abcabc abcabc
    ,   --abc                                 abcabc
                                              Required.
  -len, --length                           -> Default: \"10\"
  -f                                       -> Flag
                                              whatever
                                              Default: false
  -                                        -> data files
                                              Required.
  -h,   --help                             -> Print help for the program.

"
			],
			[
				clone $args1,
				"-a",
				"Error: Option -a is required.                                              |" . "

Usage:
  argsTest.php [options]

Options:
  -a,   -aaa                               -> abcabc abcabc
    ,   --abc                                 abcabc
                                              Required.
  -len, --length                           -> Default: \"10\"
  -f                                       -> Flag
                                              whatever
                                              Default: false
  -                                        -> data files
                                              Required.
  -h,   --help                             -> Print help for the program.

"
			],
			[
				clone $args1,
				"-a 1 -h",
				"
Usage:
  argsTest.php [options]

Options:
  -a,   -aaa                               -> abcabc abcabc
    ,   --abc                                 abcabc
                                              Required.
  -len, --length                           -> Default: \"10\"
  -f                                       -> Flag
                                              whatever
                                              Default: false
  -                                        -> data files
                                              Required.
  -h,   --help                             -> Print help for the program.

"
			]
		];
		
		$return = [];
		foreach ($ret as $i=>$r)
			$return[] = [$i, $r[0], $r[1], $r[2]];
		
		return $return;
	}
}

if (isset($argv) and basename($argv[0]) == basename(__FILE__)) {
	
	$args  = new argsTest();
	$data  = $args->testThemData();
	$prog  = $argv[0];
	$index = @$argv[1];
	
	if (!preg_match("/^\d+$/", $index)) {
		echo "Usage: {$argv[0]} <Index of pw\Args\Args instance> [args ...]\n";
		exit;
	}
	array_shift ($argv); // progname
	array_shift ($argv); // index
	$index   = intval ($index);
	array_unshift ($argv, $prog);
	$argsins = $data[$index][1];
	$argsins->done($argv);
	echo json_encode ($argsins->get(), JSON_PRETTY_PRINT);
}