<?php
/**
 * Subcmd class for Args
 * @author pwwang <pwwang@pwwang.com>
 */
namespace pw\Args;
use pw\Utils\Utils;

/**
 * The Command class extends Args.
 * Handling the command (and their options) of a program.
 */
class Command extends Args {
	/**
	 * @ignore
	 */
	private $name;
	/**
	 * @ignore
	 */
	private $alias;
	/**
	 * @ignore
	 */
	private $args;
	/**
	 * @ignore
	 */
	private $matched;
	/**
	 * @ignore
	 */
	private $group;
	
	/**
	 * @ignore
	 */
	public function __construct ($name, $args) {
		$this->name      = $name;
		$this->alias     = [];
		$this->args      = $args; // parent
		$this->group     = "Available commands";
		
		$check = $args->_checkCmdName ($name);
		if (is_string($check))
			Helper::throwEx ($check);
			
		$this->isCommand = true;
		parent::__construct ();
	}
	
	/**
	 * @ignore
	 */
	public function getName () {
		return $this->name;
	}
	
	/**
	 * Set the group of the command
	 * @api
	 * @param	string	$group	The group name
	 * @return	Command			$this
	 */
	public function group ($group) {
		$this->group = $group;
		$this->args->_addCmdGroup ($group);
		return $this;
	}
	
	/**
	 * @ignore
	 */
	public function getGroup () {
		return $this->group;
	}
	
	/**
	 * Set the alias of the command
	 * @api
	 * @param	string	$aka	The alias of the command
	 * @return	Command			$this
	 */
	public function aka ($aka) {
		$check = $this->args->_checkCmdName ($aka);
		if (is_string($check))
			Helper::throwEx ($check);
			
		$this->alias[] = $aka;
		$names = $this->name . (empty($this->alias) ? "" : "|" . implode("|", $this->alias));
		$this->args->_adjustLen(0, strlen($names) - 2);
		return $this;
	}
	
	/**
	 * The alias of function $this->aka($aka)
	 * @api
	 * @param	string	$aka	The alias of the command
	 * @return	Command			$this
	 */
	public function alias ($aka) {
		return $this->aka ($aka);
	}
	
	/**
	 * @ignore
	 */
	public function getAlias () {
		return $this->alias;
	}
	
	/**
	 * @ignore
	 */
	public function isMe ($cmd) {
		if ($this->name == $cmd) return true;
		foreach ($this->alias as $alias) {
			if ($alias == $cmd) {
				$this->matched = $cmd;
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Return to its parent command (program)
	 * @api
	 * @return	Args|Command		The parent command
	 */
	public function end () {
		return $this->args;
	}
	
	/**
	 * @ignore
	 */
	public function getHelpInfo () {
		$alias = (empty($this->alias) ? "" : "|" . implode("|", $this->alias));
		
		$cmds = "  " . Utils::color($this->name, "bold") . str_pad (
			$alias,
			$this->args->_getLen ("name") + $this->args->_getLen("long") - strlen($this->name),
			" ",
			STR_PAD_RIGHT
		);
		
		$descs    = $this->desc;
		$ret      = "";
		for ($i=0; $i<max(1, sizeof($descs)); $i++) {
			$ret .= $i == 0
					? $cmds . " -> "
					: str_repeat(" ", $this->args->_getLen ("name") + $this->args->_getLen ("long") + 6);
			$ret .= isset($descs[$i])
					? $descs[$i]
					: "";
			$ret .= PHP_EOL;
		}
		return $ret;
	}
	/**
	 * @ignore
	 */
	protected function addHelpOpts () {
		if (in_array($this->name, $this->args->_getHelpCmds()))
			return;
			
		parent::addHelpOpts();
	}
}