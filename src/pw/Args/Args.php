<?php
/**
 * A php command line arguments handling library
 * @link https://bitbucket.org/pwwang/args.php
 * @author pwwang <pwwang@pwwang.com>
 */
namespace pw\Args;
use pw\Utils\Utils;

/**
 * The basic pw\Args\Args class.
 * class pw\Args\Command will be extended from this one.  
 * It implements \ArrayAccess, so that the values of options can be accessed as  
 * ```php
 * $args = new pw\Args\Args();
 * // do some configuration ...
 * // to access the value:
 * $args["a"] // for option -a
 * $args["command.a"] // for option -a of command "command"
 * ```
 */
class Args implements \ArrayAccess {
	
	////////////////////////////////     constants      ///////////////////////
	/**
	 * The option desciption start position, default: 40
	 * @ignore
	 */
	const DESC_STARTPOS = 40;
	
	/**
	 * Description for help option: "Print help for the program."
	 * @ignore
	 */
	const HELP_OPT_DESC = "Print help for the program.";
	
	/**
	 * Description for help command: "Print help for the command."
	 * @ignore
	 */
	const HELP_CMD_DESC = "Print help for the command.";
	
	////////////////////////////////     properties      ///////////////////////
	/**
	 * The final values of each options
	 * @ignore
	 */
	protected $values;
	
	/**
	 * The usage of the program
	 * @ignore
	 */
	protected $usage;
	
	/**
	 * The desciption of the program
	 * @ignore
	 */
	protected $desc;
	
	/**
	 * Command to show help if in subcommand mode
	 * @ignore
	 */
	protected $helpCmds;
	
	/**
	 * @ignore
	 */
	protected $helpCmdsGlobal;
	
	/**
	 * Option to show help 
	 * @ignore
	 */
	protected $helpOpts;
	
	/**
	 * @ignore
	 */
	protected $helpOptsGlobal;
	
	// Some flags

	/**
	 * whether to show help on bald (no arguments offered)
	 * @ignore
	 */
	protected $baldHelp;
	
	/**
	 * @ignore
	 */
	protected $baldHelpGlobal;
	
	/**
	 * whether to show warnings
	 * @ignore
	 */
	protected $showWarns;
	
	/**
	 * @ignore
	 */
	protected $showWarnsGlobal;
	
	/**
	 * Whether show the type of option value or not
	 * @ignore
	 */
	protected $showTypes;
	
	/**
	 * @ignore
	 */
	protected $showTypesGlobal;
	
	/**
	 * check whether Args::done() is called
	 * @ignore
	 */
	protected $done;
	
	/**
	 * whether allow no gap between option name and value
	 * say: -a1
	 * @ignore
	 */ 
	protected $allowNoGap;
	
	/**
	 * @ignore
	 */
	protected $allowNoGapGlobal;
	
	/**
	 * global length
	 * |  |   maxNameLen  |  maxLongLen  |	| maxDescLen |
	 * |  -abcdefghijklm, --abcdefghijklmn -> descdescdescc
	 * |  |<-----		   maxTotaLen			  ----->|
	 * define them as member variables to keep them aligned
	 * in both general and command options helps.
	 * @ignore
	 */
	protected $maxNameLen;
	
	/**
	 * @ignore
	 */
	protected $maxLongLen;
	
	/**
	 * @ignore
	 */
	protected $maxDescLen;
	
	/**
	 * @ignore
	 */
	protected $maxTotaLen;
	
	/**
	 * All the options
	 * Normal options are stored as : <optname> => <optobj>
	 * @ignore
	 */
	protected $options;
	
	/**
	 * All the subcommands
	 * @ignore
	 */
	protected $commands;
	
	/**
	 * Current command
	 * @ignore
	 */
	protected $command;
	
	/**
	 * Whether I am a command or the root program
	 * @ignore
	 */
	protected $isCommand = false;
	
	/**
	 * The warnings
	 * @ignore
	 */
	protected $warnings;
	
	/**
	 * The errors
	 * @ignore
	 */
	protected $errors;
	
	/**
	 * The groups of options
	 * @ignore
	 */
	protected $optGroups;
	
	/**
	 * The groups of commands
	 * @ignore
	 */
	protected $cmdGroups;
	
	/**
	 * The group of the help option
	 * @ignore
	 */
	protected $helpOptGroup;
	
	/**
	 * The group of the help command
	 * @ignore
	 */
	protected $helpCmdGroup;
	
	
	////////////////////////////////     static public methods      ///////////////////////
	/**
	 * A shortcut for the construct: new pw\Args\Args ()
	 * @static
	 * @api
	 * @param	string	$configfile	The configuration file
	 * @return	Args				An Args instance
	 */
	static public function factory ($configfile = null) {
		return new self($configfile);
	}
	
	////////////////////////////////     public methods      ///////////////////////
	/**
	 * The construct
	 * @api
	 * @param	string	$configfile	The configuration file
	 */
	public function __construct ($configfile = null) {
	
		$this->desc		        = [];
		$this->helpCmds	        = ["help"];
		$this->helpCmdsGlobal   = true;
		$this->helpOpts	        = ["-h", "--help"];
		$this->helpOptsGlobal   = true;
		
		$this->options          = [];
		$this->commands         = [];
		                        
		$this->maxNameLen       = 4;  // '-h, '
		$this->maxLongLen       = 6;
		$this->maxDescLen       = 27; //max(strlen(self::HELP_CMD_DESC), strlen(self::HELP_OPT_DESC));
		$this->maxTotaLen       = 0;
		                        
		$this->usage	        = ["{prog} [options]"];
		$this->values	        = [];
		                        
		$this->baldHelp         = false;
		$this->baldHelpGlobal   = true;
		$this->showWarns        = true;
		$this->showWarnsGlobal  = true;
		$this->showTypes        = false;
		$this->showTypesGlobal  = true;
		$this->allowNoGap       = false;
		$this->allowNoGapGlobal = true;
		$this->done		        = false;
		$this->command          = '';
		$this->warnings         = [];
		$this->errors           = [];
		                        
		$this->optGroups        = [];
		$this->cmdGroups        = [];
		
		$this->helpOptGroup     = 'Options';
		$this->helpCmdGroup     = 'Available commands';
		
		// initial lengths						 ', '
		$this->_adjustLen (4);
		
		if (!$this->isCommand)
			$this->loadFromConfigFile ($configfile);
	}
	
	/**
	 * Adjust the length of each element
	 * @ignore
	 * @param	int	$namelen	The length of the option name
	 * @param	int	$longlen	The length of the option long name
	 * @param	int	$desclen	The length of the description
	 * @param	int	$totalen	The total length
	 */
	public function _adjustLen ($namelen = 0, $longlen = 0, $desclen = 0, $totalen = 0) {
		$this->maxNameLen = max($this->maxNameLen, $namelen);
		$this->maxLongLen = max($this->maxLongLen, $longlen, self::DESC_STARTPOS - $this->maxNameLen - 2);
		$this->maxDescLen = max($this->maxDescLen, $desclen, $this->maxTotaLen - 2 - $this->maxNameLen - $this->maxLongLen - 4);
		$this->maxTotaLen = max($this->maxTotaLen, $totalen, 2 + $this->maxNameLen + $this->maxLongLen + 4 + $this->maxDescLen);
		$this->maxDescLen = max($this->maxDescLen, $desclen, $this->maxTotaLen - 2 - $this->maxNameLen - $this->maxLongLen - 4);
	}
	
	/**
	 * Check the command name, whether it's available
	 * @ignore
	 * @param	string	$name	The command name
	 * @return	mixed			True if available, else return the error message
	 */
	public function _checkCmdName ($name) {
		if ($name == '')
			return "Command name cannot be empty.";
		if ($name[0] == '-')
			return "Command name should not start with '-', will be confused with options.";
			
		foreach ($this->commands as $command) {
			if ($command->isMe ($name))
				return "Command $name has been taken by {$command->getName()}.";
		}
		return true;
	}
	
	/**
	 * Check the option name, whether it's available
	 * @ignore
	 * @param	string	$name	The option name
	 * @return	mixed			True if available, else return the error message
	 */
	public function _checkOptName ($name, $long = false) {
		if ($name != '' and $name[0] == '-') {
			return "Option name cannot start with '-'. If you want to define long name, use 'long'.";
		}
		
		$nameopt = $long ? "--$name" : "-$name";
		foreach ($this->options as $option) {
			
			foreach ($option->getShorts() as $short) {
				if ($short == $name) 
					return "Option name '$name' has been taken by option -{$option->getName()}.";
			}
			
			foreach ($option->getLongs() as $longs) {
				if ($longs == $name) 
					return "Option long name '$name' has been taken by option --$longs(-{$option->getName()}).";
			}
			
			if (!$long) {
				$isme = $option->isMe ($nameopt);
				
				if ($option->getType() == 'incr' and $isme === true) {
					return "Option '$nameopt' may be confused with increment option -{$option->getName()}.";
				}
				
				if ($isme !== false and $this->allowNoGap) {
					return  "Option '$nameopt' starts with option -{$option->getName()}'s names. " .
							"This will be confused for '-a1 -ab1': a=1,ab=1 or a=1,a=b1. " .
							"To avoid this, set allowNoGap(false), or use a different option name.";
				}
				if ($option->partOfMe ($name, $long) and $this->allowNoGap) {
					return  "Option '$nameopt' is part of option -{$option->getName()}'s names. " .
							"This will be confused for '-a1 -ab1': a=1,ab=1 or a=1,a=b1. " .
							"To avoid this, set allowNoGap(false), or use a different option name.";
				}
			} else {
				$isme = $option->isMe ($nameopt);
				
				if ($isme !== false and $this->allowNoGap) {
					return  "Option '$nameopt' starts with option -{$option->getName()}'s long names. " .
							"This will be confused for '--a1 --ab1': a=1,ab=1 or a=1,a=b1. " .
							"To avoid this, set allowNoGap(false), or use a different long name.";
				}
				if ($option->partOfMe ($name, $long) and $this->allowNoGap) {
					return  "Option '$nameopt' is part of option -{$option->getName()}'s long names. " .
							"This will be confused for '--a1 --ab1': a=1,ab=1 or a=1,a=b1. " .
							"To avoid this, set allowNoGap(false), or use a different long name.";
				}
			}
		}
		return true;
	}
	
	/**
	 * Get the length of individual element
	 * @ignore
	 * @param	string	$which	The name of the element (name|long|desc|total)
	 * @return	int				The length
	 */
	public function _getLen ($which) {
		$which = ucfirst($which);
		if ($which == "Total")
			$which = "Tota";
		$var = "max{$which}Len";
		return $this->$var;
	}
	
	/**
	 * Get the property value allowNoGap
	 * @ignore
	 * @return	bool		The value of allowNoGap
	 */
	public function _getAllowNoGap () {
		return $this->allowNoGap;
	}
	
	/**
	 * Get the help commands (strings)
	 * @ignore
	 * @return	array		The help commands
	 */
	public function _getHelpCmds () {
		return $this->helpCmds;
	}
	
	/**
	 * Get the value of showTypes
	 * @ignore
	 * @return	bool		The value of showTypes
	 */
	public function _getShowTypes () {
		return $this->showTypes;
	}
	
	/**
	 * Add a option group name to the stack
	 * @ignore
	 * @param	string	$group	The group name
	 */
	public function _addOptGroup ($group) {
		if (!in_array($group, $this->optGroups))
			$this->optGroups[] = $group;
	}
	
	/**
	 * Add a command group name to the stack
	 * @ignore
	 * @param	string	$group	The group name
	 */
	public function _addCmdGroup ($group) {
		if (!in_array($group, $this->cmdGroups))
			$this->cmdGroups[] = $group;
	}
	
	/**
	 * Allow the tight mode, such as "prog -a1 -b2"
	 * @api
	 * @param	Boolean	$allow	Set true to allow, false to disallow
	 * @param	Boolean	$global	Whether apply this value to the child commands
	 * @return	Args			$this
	 */
	public function allowNoGap ($allow = true, $global = true) {
		$this->allowNoGap = $allow;
		$this->allowNoGapGlobal = $global;
		return $this;
	}
	
	/**
	 * Whether show the type of the option value
	 * @api
	 * @param	Boolean	$showtype	True to show, false to hide
	 * @param	Boolean	$global		Whether apply this value to the child commands
	 * @return	Args				$this
	 */
	public function showTypes ($showtype = true, $global = true) {
		$this->showTypes = $showtype;
		$this->showTypesGlobal = $global;
		return $this;
	}
	
	/**
	 * Implement offsetSet of \ArrayAccess
	 * Set value for $offset of $this->values
	 * @api
	 * @param	string|int	$offset	The offset of the array
	 * @param	mixed		$value	The value to set
	 */
	public function offsetSet($offset, $value) {
		$this->values[$offset] = $value;
	}
	
	/**
	 * Implement offsetExists of \ArrayAccess
	 * Check whether a key $var exists for $this->values
	 * @api
	 * @param	string|int	$var	The key
	 * @return	bool				True if exists, false otherwise
	 */
	public function offsetExists($var) {
		return isset($this->values[$var]);
	}
	
	/**
	 * Implement offsetUnset of \ArrayAccess
	 * Unset a key in $this->values
	 * @api
	 * @param	string|int	$var	The key
	 */
	public function offsetUnset($var) {
		unset($this->values[$var]);
	}
	
	/**
	 * Implement offsetGet of \ArrayAccess
	 * Implement the function that use $args["a"] to get the value of option -a
	 * @api
	 * @param	string|int	$var	The key
	 * @return	mixed			The value of the option
	 */
	public function offsetGet($var) {
		if (!$this->done)
			Helper::throwEx("Forget to call ". __CLASS__ ."::done() ?");
            
        if (!isset($this->values[$var]))
			Helper::throwEx("No such option '$var'");
            
		return $this->values[$var];
	}
	
	/**
	 * Add an option for this command/program
	 * @api
	 * @param	String	$name	The name of the option
	 * @return	Option			An instance of pw\Args\Option
	 */
	public function option ($name = "") {		
		//                        [-x, ]
		$this->options[$name] = new Option($name, $this);
		$this->_adjustLen(strlen($name)+3, 0, 0, 0);
		return $this->options[$name];
	}
	
	/**
	 * Alias of $this->option($name)
	 * @api
	 * @param	String	$name	The name of the option
	 * @return	Option			An instance of pw\Args\Option
	 */
	public function add ($name = "") {
		return $this->option ($name);
	}
	
	/**
	 * Alias of $this->option($name)
	 * @api
	 * @param	String	$name	The name of the option
	 * @return	Option			An instance of pw\Args\Option
	 */
	public function opt ($name = "") {
		return $this->option ($name);
	}
	
	/**
	 * Add a command for this command/program
	 * @api
	 * @param	string	$name	The name of the command
	 * @return	Command			An instance of pw\Args\Command
	 */
	public function command ($name) {

		// in case the name is too long
		$this->_adjustLen(0, strlen($name) - $this->maxNameLen, 0, 0);
		$this->commands[$name] = new Command($name, $this);
		if ($this->allowNoGapGlobal)
			$this->commands[$name]->allowNoGap ($this->allowNoGap, $this->allowNoGapGlobal);
		if ($this->helpCmdsGlobal)
			$this->commands[$name]->setHelpCmds($this->helpCmds, $this->helpCmdsGlobal);
		if ($this->helpOptsGlobal)
			$this->commands[$name]->setHelpOpts($this->helpOpts, $this->helpOptsGlobal);
		if ($this->baldHelpGlobal)
			$this->commands[$name]->helpOnBald($this->baldHelp, $this->baldHelpGlobal);
		if ($this->showWarnsGlobal)
			$this->commands[$name]->showWarnings($this->showWarns, $this->showWarnsGlobal);
		if ($this->showTypesGlobal)
			$this->commands[$name]->showTypes($this->showTypes, $this->showTypesGlobal);
		return $this->commands[$name];
	}
	
	/**
	 * Get the command instance by its name
	 * @api
	 * @param	string	$name	The name of the command
	 * @return	Command			The instance of pw\Args\Command
	 */
	public function getCommand ($name) {
		foreach ($this->commands as $command) {
			if ($command->isMe($name))
				return $command;
		}
		Helper::throwEx ("No such command: $name.");
	}
	
	/**
	 * Get the option instance by its name
	 * @api
	 * @param	string	$name	The name of the option
	 * @return	Option			The instance of pw\Args\Option
	 */
	public function getOption ($name) {
		foreach ($this->options as $option) {
			if ($option->isMe ("-$name") === true)
				return $option;
		}
		Helper::throwEx ("No such option: $name.");
	}
	
	/**
	 * The the value of an option/all options
	 * @api
	 * @param	string|null	$key	The option name/alias/long name
	 * @return	mixed				The value of the options or the array of all options if $key == null
	 */
	public function get($key = null) {
		if (!$this->done)
			Helper::throwEx("Forget to call " . __CLASS__ . "::done() ?");
		if (!is_null($key))
			return $this->values[$key];
		return $this->values;
	}
	
	/**
	 * Set the usage information of the program/command
	 * @api
	 * @param	string	$usage	The usage.
	 * 	You can simply use "\n" to separate lines. 
	 *  The white spaces will be trimmed for each line.
	 *  If you need indents of some lines, use heading '_'.
	 *  You can use placeholder {prog}/{command} to stand for the programe/command name.
	 * @return	Args			$this
	 */
	public function usage($usage) {
		$this->usage = Helper::replaceHeadingUnderline(array_map("trim", explode("\n", $usage)));
		return $this;
	}
	
	/**
	 * Set the description of the program/command
	 * @api
	 * @param	string	$desc	The description.
	 * 	You can simply use "\n" to separate lines.
	 *  The white spaces will be trimmed for each line.
	 *  If you need indents of some lines, use heading '_'.
	 * @return	Args			$this
	 */
	public function desc($desc) {
		$this->desc = Helper::replaceHeadingUnderline(array_map("trim", explode("\n", $desc)));
		$this->_adjustLen(0, 0, 0, max(array_map("strlen", $this->desc)));
		return $this;
	}
	
	/**
	 * Set the order of the option groups.
	 * @api
	 * @param	string|array	$groups	The groups in order.
	 *  Note that there is not pre-check for the non-exist groups.
	 * @return	Args			$this
	 */
	public function optGroupOrder ($groups) {
		$optGroups  = is_string($groups) ? preg_split("/\s*,\s*/", $groups) : $groups;
		/*$diffGroups = array_diff ($optGroups, $this->optGroups);
		if (!empty($diffGroups)) {
			Helper::throwEx ("Unknown option group(s): " . implode(", ", $diffGroups));
		}
		$diffGroups = array_diff ($this->optGroups, $optGroups);
		$this->optGroups = array_merge ($optGroups, $diffGroups);*/
		return $this;
	}
	
	/**
	 * Set the order of the command groups.
	 * @api
	 * @param	string|array	$groups	The groups in order.
	 *  Note that there is not pre-check for the non-exist groups.
	 * @return	Args			$this
	 */
	public function cmdGroupOrder ($groups) {
		$cmdGroups  = is_string($groups) ? preg_split("/\s*,\s*/", $groups) : $groups;
		/*$diffGroups = array_diff ($cmdGroups, $this->cmdGroups);
		if (!empty($diffGroups)) {
			Helper::throwEx ("Unknown command group(s): " . implode(", ", $diffGroups));
		}
		$diffGroups = array_diff ($this->cmdGroups, $cmdGroups);
		$this->cmdGroups = array_merge ($cmdGroups, $diffGroups);*/
		return $this;
	}
	
	/**
	 * Set the group that help option belonging to
	 * @api
	 * @param	string	$group	The group name
	 * @return	Args			$this
	 */
	public function helpOptGroup ($group) {
		$this->helpOptGroup = $group;
		return $this;
	}
	
	/**
	 * Set the group that help command belonging to
	 * @api
	 * @param	string	$group	The group name
	 * @return	Args			$this
	 */
	public function helpCmdGroup ($group) {
		$this->helpCmdGroup = $group;
		return $this;
	}
	
	/**
	 * Customize the help command names (default 'help')
	 * @api
	 * @param	string|array	$cmds	The command names
	 * @param	Boolean			$global	Whether also set the help commands for the child commands
	 * @return	Args					$this
	 */
	public function setHelpCmds ($cmds, $global = true) {
		$this->helpCmds = is_string($cmds) ? preg_split("/\s*,\s*/", $cmds) : $cmds;
		$this->_adjustLen(0, strlen(implode("|", $this->helpCmds)) - $this->maxNameLen, 0, 0);
		$this->helpCmdsGlobal = $global;
		return $this;
	}
	
	/**
	 * Customize the help option names (default '-h, --help')
	 * @api
	 * @param	string|array	$opts	The option names
	 * @param	Boolean			$global	Whether also set the help options for the child commands
	 * @return	Args					$this
	 */
	public function setHelpOpts ($opts, $global = true) {
		$this->helpOpts = is_string($opts) ? preg_split("/\s*,\s*/", $opts) : $opts;
        if (empty($this->helpOpts))
            Helper::throwEx ('Help options cannot be empty.');
        
        $this->helpOpts = array_map (function($v){
            if ($v == '') return '-';
            if ($v[0] != '-') return "-$v";
            return $v;
        }, $this->helpOpts);
        
        $hasShort = array_sum ( array_map (function($v){
            $len = strlen ($v);
            if ($len == 0) return 0;
            if ($len == 1) return intval($v == '-');
            // $len > 1
            return intval($v[0] == '-' and $v[1] != '-');
        }, $this->helpOpts) );
        if ($hasShort == 0)
            Helper::throwEx ('Help option must have at least one short name.');
            
		$this->_adjustLen(
			strlen($this->helpOpts[0])+2,
			sizeof($this->helpOpts) > 1
				? max(array_map("strlen", array_slice($this->helpOpts, 1)))
				: 0
		);
		$this->helpOptsGlobal = $global;
		return $this;
	}
	
	/**
	 * Whether allow to show the help information if no arguments offered in the command line.
	 * @api
	 * @param	Boolean	$flag	True to allow, false to disallow
	 * @param	Boolean	$global	Whether also set the flag to the child commands
	 * @return	Args			$this
	 */
	public function helpOnBald ($flag = true, $global = true) {
		$this->baldHelp = $flag;
		$this->baldHelpGlobal = $global;
		return $this;
	}
	
	/**
	 * Whether show warnings or not
	 * @api
	 * @param	Boolean	$flag	True to show, false to hide
	 * @param	Boolean	$global	Whether also set the flag to the child commands
	 * @return	Args			$this
	 */
	public function showWarnings ($flag = true, $global = true) {
		$this->showWarns = $flag;
		$this->showWarnsGlobal = $global;
		return $this;
	}
	
	/**
	 * After the parsing, finish the value assignment
	 * @api
	 * @param	string|array	$_argv The arguments. If it is not offered, will use $argv.
	 * @return	Args			$this
	 */
	public function done ($_argv = null) {
		$this->done = true;
		
		global $argv;
		$args = $argv;
		if (!is_null($_argv))
			$args = is_string($_argv) ? Helper::getArgv ($_argv) : $_argv;
		
		$this->addHelpOpts ();
		$this->addHelpCmdsIfNeeded ();
		
		$progname = $this->parse ($args);
		// adjust desc len
		
		// no command mode
		$help = $this->getHelpInfoWithoutCommand ($progname);
		
		if (!empty ($this->commands)) {
			$help .= $this->getHelpCmdList ();
		}
		$help .= PHP_EOL;
		
		$helpopt = $this->getHelpOpt();
		
		if ((!is_null($helpopt) and $helpopt->getValue() === true) or (
			$this->baldHelp and sizeof($args) == 1) or (
			in_array($this->command, $this->helpCmds))) { 
			fwrite (STDERR, $help);
			exit;
		}
		foreach ($this->options as $name => $option) {
			if ($option->getRequired() and is_null($option->getValue())) {
				$this->_addError ("Option -$name is required.");
			}
			if (is_null($option->getValue())) {
				$option->setValue ($option->getDefault());
			}
			
			$option->dumpValue ($this->values);
		}
		// just show one error
		// multiple errors will be confused.
		foreach ($this->errors as $error) {
			fwrite (STDERR, $error);
			fwrite (STDERR, $help);
			exit;
		}
		if ($this->showWarns) {
			foreach ($this->warnings as $warning)
				fwrite (STDERR, $warning);
		}
		// dump command values
		if ($this->command != '') {
			$curcmd = $this->getCommand($this->command);
			$cmdvals = $curcmd->get();
			foreach ($cmdvals as $key => $value) {
				$this->values["{$curcmd->getName()}.$key"] = $value;
				foreach ($curcmd->getAlias() as $alias) {
					$this->values["$alias.$key"] = $value;
				}
			}
		}
		
		return $this;
	}
	
	////////////////////////////////     private methods      ///////////////////////
	
	/**
	 * Parse the arguments without command.
	 * (command with arguments will be parsed by the child Command instance)
	 * @ignore
	 * @param	string	$progname	The program/command name
	 * @return	Args				$this
	 */
	private function getHelpInfoWithoutCommand ($progname) {
		$ret = "";
		$ret .= $this->getHelpUsage ($progname);
		$ret .= $this->getHelpDesc  ();
		$ret .= $this->getHelpOptions ();
		return $ret;
	}
	
	/**
	 * Get the usage information
	 * @ignore
	 * @param	string	$progname	The program/command name
	 * @return	string				The formatted usage information
	 */
	private function getHelpUsage($progname) {
		$usages = array_map (function($u) use ($progname) {
			return str_replace(["{prog}", "{command}"], Utils::color($progname, "bold"), $u);
		}, $this->usage);
		$maxlen = max (array_map("strlen", $usages));
		$ret    = PHP_EOL . Utils::color("Usage:", "yellow") . PHP_EOL;
		$ret   .= "  "  . implode("\n  ", $usages) . PHP_EOL;
		
		$this->_adjustLen (0, 0, 0, $maxlen);
		return $ret;
	}
	
	/**
	 * Get description for help information
	 * @ignore
	 * @return	string		The formatted description information for help
	 */
	private function getHelpDesc() {
		$title    = PHP_EOL . Utils::color("Description:", "yellow") . PHP_EOL;
		$ret      = "";
		if (!empty($this->desc)) {
			$ret  = $title;
			$ret .= "  "  . implode("\n  ", $this->desc) . PHP_EOL;
		}
		return $ret;
	}
	
	/**
	 * Get options for help information
	 * @ignore
	 * @return	string		The formatted option information for help
	 */
	private function getHelpOptions () {
		$helpopt = $this->getHelpOpt();
		if (is_null($helpopt)) return '';
		
		$groupOpts = array_fill_keys ($this->optGroups, []);
		
		foreach ($this->options as $option) 
			$groupOpts[$option->getGroup()][] = $option->getHelpInfo ();
		
		
		$ret = "";
		foreach ($groupOpts as $group => $helpinfos) {
			$ret .= PHP_EOL . Utils::color ("$group:", "yellow") . PHP_EOL;
			$ret .= implode ('', $helpinfos);
		}
		
		return $ret;
	}
	
	/**
	 * Get the command list for help information
	 * @ignore
	 * @return	string		The command list (including their alias and descriptions) for help information
	 */
	private function getHelpCmdList () {
		
		$groupCmds = array_fill_keys ($this->cmdGroups, []);
		
		foreach ($this->commands as $command)
			$groupCmds[$command->getGroup()][] = $command->getHelpInfo ();
			
		$ret = "";
		foreach ($groupCmds as $group => $helpinfos) {
			$ret .= PHP_EOL . Utils::color ("$group:", "yellow") . PHP_EOL;
			$ret .= implode ('', $helpinfos);
		}
		
		return $ret;
	}	
	
	/**
	 * Parse the arguments without command.
	 * (command with arguments will be parsed by the child Command instance)
	 * @ignore
	 * @param	array|string	$args	The arguments
	 */
	private function parseWithoutCommand ($args) {
		$rest = []; 
		for ($i=0; $i<sizeof($args); $i++) {
			$arg = $args[$i];
			$break = false;
			foreach ($this->options as $option) {
				
				$isme = $option->isMe ($arg);
				//if ($option->getType() == 'incr') Utils::dump($isme, $arg);
				if ($isme === false) continue;
				//Utils::dump($option->getName ());
				// have something left in last match
				if (!empty ($rest)) {
					$this -> _addWarning ("Unknown option(s)/value(s): " . implode(", ", $rest));
					$rest = [];
				}
				$optArgs = [$arg];
				
				switch ($option->getType ()) {
					case 'bool':
						if (isset($args[$i+1]) and (
							in_array($args[$i+1], explode(',', Option::BOOL_TRUES)) or
							in_array($args[$i+1], explode(',', Option::BOOL_FALSES))
						)) {
							$optArgs[] = $args[++$i];
						}
						break;
					case 'flag':
						break;
					case 'array':
						$jumpTo = $i;
						for ($j=$i+1; $j<sizeof($args); $j++) {
							if ($args[$j][0] != '-') { // not an option
								$optArgs[] = $args[$j];
								$jumpTo = $j;
							} else {
								break;
							}
						}
						$i = $jumpTo;
						break;
					case 'incr':
						if (strlen($arg) >= 2 and $arg[0] == '-' and $arg[1] == '-') { // long
							if ($isme == true) $optArgs[] = $args[++$i];
						}
						break;
					default:
						if ($isme === true and isset($args[$i+1]) and $args[$i+1][0] != '-')
							$optArgs[] = $args[++$i];
						break;
				}
				$option->done ($optArgs);
				$break = true;
				break;
			}
			if (!$break) $rest[] = $arg;
		}
		if (!empty($rest)) {
			if (isset($this->options[""])) {
				array_unshift ($rest, '-');
				$this->options[""] -> done ($rest);
			} else {
				$this->_addWarning ("Unknown option(s)/value(s): " . implode(", ", $rest));
			}
		}
	}
	
	/**
	 * Parse the arguments
	 * @ignore
	 * @param	array|string	$args	The arguments
	 * @return	string			The program/command name
	 */
	private function parse ($args) {
		$progname = basename(array_shift ($args));
		if (empty ($this->commands)) {
			$this->parseWithoutCommand ($args);
		} else {
			$commandIndex = 0;
			$commandObj   = null;
			foreach ($args as $i => $arg) {
				foreach ($this->commands as $command) {
					if ($command->isMe ($arg)) { 
						$commandObj = $command;
						$this->command = $arg;
						$this->values["-"] = $commandObj->getName();
						break;
					}
				}
				if (!is_null ($commandObj)) {
					$commandIndex = $i;
					break;
				}
			}
			if (!is_null ($commandObj)) {
				$this->parseWithoutCommand (array_slice ($args, 0, $commandIndex));
				$commandObj -> done (array_slice ($args, $commandIndex));
			}
			else {
				$this->parseWithoutCommand ($args);
				$this->_addError ("No command specified.");
			}
		}
		return $progname;
	}
	
	/**
	 * Add the help options
	 * @ignore
	 */
	protected function addHelpOpts () {
		$helpShorts = [];
		$helpLongs  = [];
		foreach ($this->helpOpts as $helpOpt) {
			if (strpos($helpOpt, '--') === 0)
				$helpLongs[] = substr ($helpOpt, 2);
			elseif ($helpOpt[0] == '-')
				$helpShorts[] = substr($helpOpt, 1);
		}
		
		if (empty($helpShorts))
			$helpShorts[] = '?';
		
		$helpOption = $this->option (array_shift($helpShorts));
		foreach ($helpShorts as $helpShort)
			$helpOption->aka ($helpShort);
		foreach ($helpLongs as $helpLong)
			$helpOption->long ($helpLong);
		$helpOption -> desc ($this->isCommand ? self::HELP_CMD_DESC : self::HELP_OPT_DESC)
					-> group ($this->helpOptGroup)
					-> flag ();
	}
	
	/**
	 * Get the help Option instance
	 * @ignore
	 * @return	Option|null		The help Option instance.
	 *  Null if there is no help options, i.e. the help command
	 */
	private function getHelpOpt () {
		$helpShorts = [];
		foreach ($this->helpOpts as $helpOpt) {
			if (strpos($helpOpt, '--') === 0)
				continue;
			elseif ($helpOpt[0] == '-') {
				$helpShorts[] = substr($helpOpt, 1);
			}
		}
		
		return isset($this->options[$helpShorts[0]]) ? $this->options[$helpShorts[0]] : null;
	}
	
	/**
	 * Add help commands if needed (any command added)
	 * @ignore
	 */
	private function addHelpCmdsIfNeeded () {
		if (!empty($this->commands)) {
			$hcmd = $this->helpCmds[0];
			$helpCmd = $this->command($hcmd)
							->usage ('{prog} help')
							->group ($this->helpCmdGroup)
							->desc  ($this->isCommand ? self::HELP_CMD_DESC : self::HELP_OPT_DESC);
			//$helpCmd -> done ();
		}
	}
	
	/**
	 * Get the help Command instance
	 * @ignore
	 * @return	Command		The instance of help command
	 */
	private function getHelpCmd () {
		return $this->commands [$this->helpCmds[0]];
	}
	
	/**
	 * Load the configuration from a file
	 * @ignore
	 * @param	string	$configfile	The configuration file
	 * @return	Args				$this
	 */
	private function loadFromConfigFile ($configfile) {
		global $argv;
		if (is_null($configfile)) {
			$configfile = dirname($argv[0]) . DIRECTORY_SEPARATOR . pathinfo ($argv[0], PATHINFO_FILENAME) . ".args.json";
		}
		
		if (!is_file ($configfile)) return;
		$config = json_decode (file_get_contents($configfile), true);
		
		if (json_last_error() != JSON_ERROR_NONE)
			Helper::throwEx ("JSON parse error from $configfile: " . json_last_error_msg());
			
		$this->loadFromArray ($config);
		return $this;
	}
	
	/**
	 * Load the configuration from a PHP array
	 * @api
	 * @param	array	$config	The configuration
	 * @return	Args			$this
	 */
	public function loadFromArray ($config) {
		foreach ($config as $key => $value) {
			switch ($key) {
				case "desc":
					$this->desc ($value);
					break;
				case "helpCmds":
					$this->setHelpCmds ($value, (isset($config["helpCmdsGlobal"]) ? $config["helpCmdsGlobal"] : true));
					break;
				case "helpCmdsGlobal":
					break;
				case "helpOpts":
					$this->setHelpOpts ($value, (isset($config["helpOptsGlobal"]) ? $config["helpOptsGlobal"] : true));
					break;
				case "usage":
					$this->usage ($value);
					break;
				case "helpOnBald":
					$this->helpOnBald ($value, (isset($config["helpOnBaldGlobal"]) ? $config["helpOnBaldGlobal"] : true));
					break;
				case "showWarnings":
					$this->showWarnings ($value, (isset($config["showWarningsGlobal"]) ? $config["showWarningsGlobal"] : true));
					break;
				case "showTypes":
					$this->showTypes ($value, (isset($config["showTypesGlobal"]) ? $config["showTypesGlobal"] : true));
					break;
				case "allowNoGap":
					$this->allowNoGap ($value, (isset($config["allowNoGapGlobal"]) ? $config["allowNoGapGlobal"] : true));
					break;
				case "helpOptGroup":
					$this->helpOptGroup ($value);
					break;
				case "helpCmdGroup":
					$this->helpCmdGroup ($value);
					break;
				case "optGroupOrder":
					$this->optGroupOrder ($value);
					break;
				case "cmdGroupOrder":
					$this->cmdGroupOrder ($value);
					break;
				case "options":
					foreach ($value as $optname => $optconfig) 
						$this->option($optname)->loadFromArray ($optconfig);
					break;
				case "commands":
					foreach ($value as $cmdname => $cmdconfig)
						$this->command($cmdname)->loadFromArray ($cmdconfig);
					break;
				// command
				case "alias":
					foreach ($value as $alias)
						$this->alias ($alias);
					break;
				case "group":
					$this->group ($value);
					break;
				
				default:
					break;
			}
		}
		return $this;
	}

	/**
	 * Add an error to the stack
	 * @ignore
	 * @param	string	$error	The error
	 */
	public function _addWarning ($warning) {
		$warning = "Warning: $warning";
		$this->_adjustLen (0, 0, 0, strlen($warning));
		$this->warnings[] = Utils::color (
			str_pad($warning, $this->maxTotaLen + 2, " ", STR_PAD_RIGHT),
			"bg_yellow",
			"black"
		) . PHP_EOL;
	}
	
	/**
	 * Add an error to the stack
	 * @ignore
	 * @param	string	$error	The error
	 */
	public function _addError ($error) {
		$warning = "Error: $error";
		$this->_adjustLen (0, 0, 0, strlen($warning));
		$this->errors[] = Utils::color (
			str_pad($warning, $this->maxTotaLen + 2, " ", STR_PAD_RIGHT),
			"bg_light_red",
			"white"
		) . PHP_EOL;
	}
}
