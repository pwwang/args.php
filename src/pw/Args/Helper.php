<?php
/**
 * @ignore
 */
namespace pw\Args;
use pw\Utils\Utils;

/**
 * @ignore
 */
class Helper {
	
	static public function throwEx ($msg) {
		throw new \Exception(Utils::color($msg, "light_red"));
	}
	
	static public function getArgv ($string) {
		//preg_match_all ('/(?<=^|\s)([\'"]?)(.+?)(?<!\\\\)\1(?=$|\s)/', $string, $ms);
		//                                      ^^^^^^^^^ to be fixed \\' is not escaping '
		// refer to: http://stackoverflow.com/questions/5695240/php-regex-to-ignore-escaped-quotes-within-quotes
		preg_match_all ('/(?<=^|\s)([\'"]?)([^"\\\\]*?(?:\\\\.[^"\\\\]*)*)\1(?=$|\s)/', $string, $ms);
		return $ms[2];
	}
	
	static public function trimOptValue ($value) {
		if (preg_match ('/^=?([\'"]?)(.+)\1$/', $value, $ms));
			return $ms[2];
		return $value;
	}
	
	static public function isTrue ($str) {
		return in_array($str, explode(",", Option::BOOL_TRUES));
	}
	
	static public function isFalse ($str) {
		return in_array($str, explode(",", Option::BOOL_FALSES));
	}
	
	static public function replaceHeadingUnderline ($str) {
		if (is_string($str)) {
			if (!preg_match("/^(_+)/", $str, $ms)) return $str;
			$len = strlen ($ms[1]);
			return str_repeat (" ", $len) . substr($str, $len);
		} else {
			return array_map ([__CLASS__, 'replaceHeadingUnderline'], $str);
		}
	}
}