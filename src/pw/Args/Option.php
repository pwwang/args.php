<?php
/**
 * Option class for Args
 * @author pwwang <pwwang@pwwang.com>
 */
namespace pw\Args;
use pw\Utils\Utils;

/**
 * Option class
 */
class Option {
	
	/**
	 * bool (true) options
	 * @var string
	 * @value "1,true,T,True,TRUE,Yes,YES,Y"
	 */
	const BOOL_TRUES  = "1,true,T,True,TRUE,Yes,YES,Y";
	/**
	 * bool (true) options
	 * @var string
	 * @value "0,false,F,False,FALSE,No,NO,N"
	 */
	const BOOL_FALSES = "0,false,F,False,FALSE,No,NO,N";
    
	/**
	 * @ignore
	 */
	private $name;
	/**
	 * @ignore
	 */
    private $type;
	/**
	 * @ignore
	 */
	private $shorts;
	/**
	 * @ignore
	 */
    private $longs;
	/**
	 * @ignore
	 */
    private $desc;
	/**
	 * @ignore
	 */
    private $required;
	/**
	 * @ignore
	 */
	private $default;
	/**
	 * @ignore
	 */
	private $defaultStr;
	/**
	 * @ignore
	 */
	private $value;
	/**
	 * @ignore
	 */
	private $command;
	/**
	 * @ignore
	 */
	private $rule;
	/**
	 * @ignore
	 */
	private $ruleError;
	/**
	 * @ignore
	 */
	private $matched; // matched flag
	/**
	 * @ignore
	 */
	private $regexp;
	/**
	 * @ignore
	 * @var int
	 */
	private $incrMax;
	/**
	 * @ignore
	 */
	private $group;
	
	/**
	 * The function alias
	 * @staticvar
	 * @var array
	 */
	static private $FUNCS = [
		"short"     => "aka",
		"alias"     => "aka",
		"default"   => "default_",
		"must"      => "obey",
		"follow"    => "obey",
		"callback"  => "obey",
		"command"   => "end",
		"require"	=> "require_",
		"req"   	=> "require_",
		"required"	=> "require_",
		"array"  	=> "array_",
		"boolean"   => "bool_",
		"bool"      => "bool_",
		"increment" => "incr",
		"string"    => "string_"
	];
	
    /**
	 * @ignore
	 */
    public function __construct ($name, $command) {
		$this->command    = $command;
        $this->name       = $name;
        $this->type       = "string";
		$this->shorts     = [$name];
		$this->longs      = [];
		$this->desc       = [];
        $this->required   = false;
		$this->default    = null;
		$this->value      = null;
		$this->rule       = null;
		$this->ruleError  = null;
		$this->matched    = null;
		$this->regexp     = null;
		$this->incrMax    = 3;
		$this->group      = "Options";
		
		$check = $this->command->_checkOptName ($name);
		if (is_string($check))
			Helper::throwEx ($check);
    }
	
	/**
	 * Call the alias of some functions
	 * @api
	 * @see self::$FUNCS
	 * @param	string	$name		The function alias
	 * @param	array	$arguments	The arguments
	 * @return	mixed				The return value of original function
	 */
	public function __call ($name, $arguments) {
		if (strpos($name, 'get') === 0) {
			$prop  = lcfirst(substr($name, 3));
			$class = new \ReflectionObject ($this);

			if (!$class->hasProperty ($prop))
				Helper::throwEx("Unknown member function $name for " . __CLASS__);
			return $this->$prop;
		}
		
		if (!isset(self::$FUNCS[$name])) {
			Helper::throwEx("Unknown member function $name for " . __CLASS__);
		}
		$func = self::$FUNCS[$name];
		// return call_user_method_array($func, $this, $arguments); // deprecated
		return call_user_func_array([$this, $func], $arguments);
	}
	
	/**
	 * Get the parent command (program)
	 * @api
	 * @return	Command|Args		The parent command
	 */
	public function end () {
		return $this->command;
	}
	
	/**
	 * @ignore
	 */
	public function is (&$option) {
		return $this->name == $option -> getName();
	}
	
	/**
	 * Set the group of the option
	 * @param	string	$group	The group name
	 * @return	Option			$this
	 */
	public function group ($group) {
		$this->group = $group;
		$this->command->_addOptGroup ($group);
		return $this;
	}
	
	/**
	 * Tell whether a option str is match this option
	 * @ignore
	 * @param	string	$optstr	The option string, ie, -a, --b, -a=2, -a3
	 * @return	mixed			true of the rest string if matched otherwise false
	 *   The rest string:
	 *   "-a" -> "", "--b" -> "", "--b'abc'" -> "'abc'"
	 *   "-a=2" -> "=2", "-a3" -> "3"
	 */
	public function isMe ($optstr) {
		if ($optstr == '') return false;
		if ($optstr[0] != '-') return false;
		
		if ($this->name == '') { // tailing values
			return $optstr == '-';
		}
		
		foreach ($this->shorts as $short) {
			if (strpos($optstr, "-$short") === 0) {
				$rest = substr($optstr, strlen($short) + 1);
				
				// flag option have to be completely matched
				if ($this->type == 'flag' and $rest != '')
					continue;
				
				if ($this->type == 'incr') { // ie, -vvv, -v matched
					//Utils::dump ($rest);
					$namelen = strlen ($short);
					$restlen = strlen ($rest); // vv
					if ($restlen % $namelen != 0) continue;
					$incrnum = floor ($restlen / $namelen);
					if ($incrnum >= $this->incrMax) continue; // if max=3, strlen(vv) max to 2 
					$vv = str_repeat($short, $incrnum); // vv
					if ($vv != $rest) continue; // not the repeated part
					$this->matched = "-$short";
					return true;
				}
				
				if (!$this->command->_getAllowNoGap() and
					$rest != '' and
					!in_array($rest[0], ['=', '\'', '"'])
				) continue;
				
				$this->matched = "-$short";
				if ($rest == '') return true;
				return $rest;
			}
		}
		
		foreach ($this->longs as $long) {
			if (strpos($optstr, "--$long") === 0) { 
				$rest = substr($optstr, strlen($long) + 2);
				if (!$this->command->_getAllowNoGap() and
					$rest != '' and
					!in_array($rest[0], ['=', '\'', '"'])
				) continue;
				
				// flag option have to be completely matched
				if ($this->type == 'flag' and $rest != '')
					continue;
					
				$this->matched = "--$long";
				if ($rest == '') return true;
				return $rest;
			}
		}

		return false;
	}
	
	/**
	 * @ignore
	 */
	private function parse ($args) {
		// $found must be true or the rest string
		$optmatched = array_shift($args);
		$found = $this->isMe ($optmatched);
		
		switch ($this->type) {
			case 'bool':
				if ($found !== true) { 
					$val = Helper::trimOptValue($found);
					if (Helper::isTrue($val))
						$this->setValue(true, true, true);
					elseif (Helper::isFalse($val))
						$this->setValue(false, true, true);
					else {
						$this->setValue(true, true, true);
						$this->command->_addWarning ("Unknown bool value: $val for option -{$this->name}, regarded as true.");
					}return $found;
				} else {
					if (!empty($args)) {
						$this->setValue (Helper::isTrue($args[0]), true, true);
					} else {
						$this->setValue (true, true, true);
					}
				}
				break;
			case 'flag':
				if ($found !== true) { // will never happen, cuz -b1 is not -b for flag
					$this->command->_addWarning ("Part of the argument matched flag option -{$this->name}, use a different option name.");
				} else {
					if (!empty($args)) { // never happens
						$this->command->_addWarning ("Flag option $optmatched should not be assigned value, regarded as true.");
					} else {
						$this->setValue (true, true, true);
					}
				}
				break;
			case 'array':
				if ($found !== true)
					$this->setValue (Helper::trimOptValue($found), false);
				$this->setValue ($args, false);
				break;
			case 'incr':
				if (strlen($optmatched) >= 2 and $optmatched[0] == '-' and $optmatched[1] == '-') { // long
					if ($found !== true) {
						$this->setValue (Helper::trimOptValue($found), true, true);
					}
					else { 
						if (empty($args) and is_null($this->value))
							$this->command->_addWarning ("No value set for option $optmatched");
						else
							$this->setValue ($args[0], true, true);
					}
				} else {  // short options
					$val = (strlen ($optmatched) - 1) / (strlen ($this->matched) - 1);
					$this->setValue ($val);
				}
				break;
			default:
				if ($found !== true) {
					$this->setValue (Helper::trimOptValue($found), true, true);
				}
				else { 
					if (empty($args) and is_null($this->value))
						$this->command->_addWarning ("No value set for option $optmatched");
					else
						$this->setValue ($args[0], true, true);
				}
				break;
		}
		
	}
	
	/**
	 * Set the values after parsing
	 * @api
	 * @param	array|string	$args	The potential values of the option
	 * @return	Option			$this
	 */
	public function done ($args) {

		$this->parse ($args);
		
		// requirements check
		if ($this->required and is_null($this->value)) {
			$this->command->_addError ("Option -{$this->name} is required.");
		} else if (!$this->required and !is_null($this->default) and is_null($this->value)) {
			$this->value = $this->default;
		}
		
		// rule check
		if (is_callable($this->rule)) {
			if (call_user_func($this->rule, $this) === false) {
				$this->command->_addError ($this->ruleError);
			}
		} 
	}
	
	/**
	 * Set the type as array
	 * @api
	 * @alias array
	 * @return	Option		$this
	 */
    public function array_ () {
        $this->type     = "array";
		return $this;
    }
	
	/**
	 * Set the type as string
	 * @api
	 * @alias string
	 * @param	regexp	$regexp	The regular expression that the value should obey.
	 *  If $regexp == null, wild string can be accepted.
	 * @return	Option			$this
	 */
	public function string_ ($regexp = null) {
		$this->type     = "string";
		$this->regexp   = $regexp;
		return $this;
	}
    
	/**
	 * Set the type as bool
	 * @api
	 * @alias bool, boolean
	 * @return	Option		$this
	 */
	public function bool_ () {
		$this->type     = "bool";
		return $this;
	}
	
	/**
	 * Set the type as increment
	 * @api
	 * @alias increment
	 * @param	int	$max	The maximum number of repeats
	 * @return	Option		$this
	 */
	public function incr ($max = 3) {
		$validincrname = true;
		foreach ($this->shorts as $short) {
			if ($short == '')
				Helper::throwEx("One of the option name is empty, the type cannot be increment.");
		}
		$this->type     = "incr";
		$this->incrMax  = $max; 
		return $this;
	}
	/**
	 * Set the type as flag
	 * @api
	 * @return	Option		$this
	 */
	public function flag () {
		$this->type     = "flag";
		$this->default  = false;
		return $this;
	}
	
	/**
	 * Set the description of this option
	 * @api
	 * @param	string	$desc	The description.
	 * @see pw\Args\Args::desc
	 * @return	Option			$this
	 */
    public function desc ($desc) {
        $this->desc = Helper::replaceHeadingUnderline(array_map("trim", explode("\n", $desc)));
		$this->command->_adjustLen(0, 0, max(array_map("strlen", $this->desc)), 0);
        return $this;
    }
    
	/**
	 * Set that the option is required
	 * @api
	 * @alias req, require, required
	 * @return	Option		$this
	 */
    public function require_ () {
        $this->required = true;
        return $this;
    }
    
	/**
	 * Set the default value of the option
	 * @api
	 * @alias	default
	 * @param	mixed	$default	The default value for the option
	 * @param	null|string	$display	The default value shown in help information
	 *  If null given, will show json_encode($default).
	 *  i.e. false will be shown as "false" instead of an empty string
	 * @return	Option				$this
	 */
    public function default_ ($default, $display = null) {
		if ($this->type == 'flag')
			Helper::throwEx ("Default value is not allowed for flag option, use bool option instead.");
		
		$this->default = $default;
		
		if ($this->type == 'bool') { 
			if (is_bool($default)) 
				$this->default = $default;

			elseif (Helper::isTrue($default))
				$this->default = true;
				
			elseif (Helper::isFalse($default))
				$this->default = false;
			
			else
				Helper::throwEx ("Invalid bool value: $default for option -{$this->name}");
		}
		
		if ($this->type == 'incr') {
			if (!preg_match ("/^\d+$/", $default)) {
				Helper::throwEx ("Value for increment option -{$this->name} should be an integer.");
			} elseif ($default < 0 or $default > $this->incrMax) {
				Helper::throwEx ("Value for increment option -{$this->name} should be from 0 to {$this->incrMax}.");
			}
		}
		
		if (is_null($display)) 
			$display = @json_encode($this->default);
			
		$this->defaultStr = $display;
		return $this;
    }
    
	/**
	 * Set the alias of the option name
	 * @api
	 * @alias	short, alias
	 * @param	string	$sname	The alias
	 * @return	Option			$this
	 */
    public function aka ($sname) {
		if ($this->type == 'incr' and $sname == '')
			Helper::throwEx ("Increment option name cannot be empty.");
		$check = $this->command->_checkOptName ($sname);
		if (is_string($check))
			Helper::throwEx ($check);
        $this->shorts[] = $sname;
		$this->command->_adjustLen(0, strlen($sname)+1, 0, 0);
		return $this;
    }
    
	/**
	 * Set the long name of the option
	 * @api
	 * @param	string	$lname	The long name
	 * @return	Option			$this
	 */
    public function long ($lname) {
		$check = $this->command->_checkOptName ($lname, true);
		if (is_string($check))
			Helper::throwEx ($check);
        $this->longs[] = $lname;
		$this->command->_adjustLen(0, strlen($lname)+2);
        return $this;
    }
    
	/**
	 * The callback for the option.
	 * The value can be changed.
	 * Value can also be validated/checked in this callback.
	 * @api
	 * @alias must, follow, callback
	 * @param	callable	$rule	The callback
	 * @example
	 *  ```php
	 *		function ($option) {
	 *			$option->setValue (intval('1')); // change the value to int type
	 *		}
	 *		function ($option) {
	 *			$value = $option->getValue();
	 *			// array option
	 *			// will print the error, e.g.: Need at least 2 values for option xx.
	 *			if (sizeof($value) < 2) return false; 
	 *			return true;
	 *		}
	 *  ```
	 * @param	string		$error	The error to show if the callback returns false
	 * @return	Option				$this
	 */
    public function obey ($rule, $error = "{option} is not in right format.") {
		if (!is_callable ($rule))
			Helper::throwEx ('Callback must be callable.');
			
        $option          = $this->matched == "-{$this->name}" ? "" : "(-{$this->name})";
		$option          = $this->matched ? $this->matched . $option : "-{$this->name}";
		$this->ruleError = str_replace("{option}", $option, $error);
		$this->rule      = $rule;
		return $this;
    }
	
	/**
	 * Set the value for this option
	 * @api
	 * @param	mixed	$value		The value to set
	 * @param	Boolean	$overriden	Override the original value or not.
	 * 	Usually the value are overriden, but for array option, the values should merged, unless $overriden == true
	 * @param	Boolean	$raiseWarning	Whether raise warning if the value is overriden.
	 */
	public function setValue ($value, $overriden = true, $raiseWarning = false) {
		if ($this->type == 'array') { // array option will never raise warning
			$this->value = (is_null($this->value) or $overriden) ? $value : array_merge ((array)$this->value, (array)$value);
		}
		else if ($this->type == 'string' and !is_null($this->regexp)) {
			if (!preg_match ($this->regexp, $value)) {
				$this->command->_addError ("Invalid value '$value' for option {$this->matched}.");
			} 
			$this->value = $value;
		} else if ($this->type == 'incr') {
			if (!preg_match ("/^\d+$/", $value)) {
				$this->command->_addError ("Invalid value '$value' for increment option {$this->matched}, should be an integer.");
			} elseif ($value < 0 or $value > $this->incrMax) {
				$this->command->_addError ("Invalid value '$value' for increment option {$this->matched}, should be from 0 to {$this->incrMax}.");
			}
			$this->value = $value;
		}
		else {
			if (!is_null($this->value) and $raiseWarning) {
				$option          = $this->matched == "-{$this->name}" ? "" : "(-{$this->name})";
				$option          = $this->matched ? $this->matched . $option : "-{$this->name}";
				$this->command->_addWarning ("Repeated non-array option $option, previous value will be overriden.");
			}
			$this->value = $value;
		}
	}
	
	/**
	 * Dump the value to an array
	 * @ignore
	 * @param	array ref	$value	The reference of an array
	 */
	public function dumpValue (&$value) {
		foreach ($this->shorts as $short) {
			$value[$short] = $this->value;
		}
		
		foreach ($this->longs as $long) {
			$value[$long] = $this->value;
		}
	}
	
	/**
	 * Get the help information for the option
	 * @ignore
	 * @return	string		The formatted help information of the option
	 */
	public function getHelpInfo () {
		$optnames = [];
		
		if ($this->type == 'incr') {
			foreach ($this->shorts as $short) {
				$name = "-$short";
				for ($i=1; $i<$this->incrMax; $i++)
					$name .= "|" . str_repeat($short, $i+1);
				$optnames[] = $name;
			}
			foreach ($this->longs as $long)   $optnames[] = "--$long";
			
			$name = array_shift ($optnames);
			$optname1st  = "  " . Utils::color($name, "bold");
			$optname1st .= str_pad (
				"",
				$this->command->_getLen ("name") + $this->command->_getLen("long") - strlen ($name),
				" ",
				STR_PAD_RIGHT
			);
			
		} else {
			foreach ($this->shorts as $short) $optnames[] = "-$short";
			foreach ($this->longs as $long)   $optnames[] = "--$long";
			
			$name = array_shift ($optnames);
			$optname1st  = "  " . Utils::color($name, "bold");
			$optname1st .= str_pad (
				(empty ($optnames) ? "" : ","),
				$this->command->_getLen ("name") - strlen($name),
				" ",
				STR_PAD_RIGHT
			);
			$optname1st .= empty ($optnames)
				? str_repeat (" ", $this->command->_getLen("Long"))
				: str_pad (array_shift($optnames), $this->command->_getLen("Long"), " ", STR_PAD_RIGHT);
		}
		
		$hoptnames = [$optname1st];
		
		foreach ($optnames as $optname) {
			$hoptname  = "  ";
			$hoptname .= str_pad(
				str_repeat(" ", strlen($this->name) + 1) . ",",
				$this->command->_getLen ("name"),
				" ",
				STR_PAD_RIGHT
			);
			$hoptname .= str_pad(
				$optname,
				$this->command->_getLen ("long"),
				" ",
				STR_PAD_RIGHT
			);
			$hoptnames[] = $hoptname;
		}
		
		$typestr = "";
		if ($this->command->_getShowTypes()) {
			$typestr = "[". ucfirst($this->type) . (($this->type == "string" and !is_null($this->regexp)) ? "(" .$this->regexp. ")" : "") . "]";
			$this->command->_adjustLen(0, 0, strlen ($typestr), 0);
			$desc    = [Utils::color($typestr, "cyan")];
		} else {
			$desc    = [];
		}
		$descs    = array_merge($desc, $this->desc);
		if ($this->required) {
			$descs[] = Utils::color ("Required.", "light_red");
		} else if (!is_null ($this->default) and $this->type != "flag") {
			$descs[] = Utils::color ("Default: " . $this->getDefaultStr(), "green");
		}
		$ret = "";
		for ($i=0; $i<max(sizeof($hoptnames), sizeof($descs)); $i++) {
			$ret .= isset($hoptnames[$i])
					? $hoptnames[$i]
					: str_repeat(" ", $this->command->_getLen ("name") + $this->command->_getLen ("long") + 2);
			$ret .= $i==0 ? " -> " : "    ";
			$ret .= isset($descs[$i])
					? $descs[$i]
					: "";
			$ret .= PHP_EOL;
		}
		return $ret;
	}
	
	/**
	 * @ignore
	 */
	public function partOfMe ($name, $long = false) {
		if ($name == '')
			return false;
			
		if (!$long) { // short
			foreach ($this->shorts as $short) {
				if ($short == '') continue;
				
				$str = $short;
				if ($this->type == 'incr') {
					for ($i=1; $i<$this->incrMax; $i++)
						$str .= $short;
				}
				if (strpos($str, $name) === 0)
					return true;
			}
		} else {
			foreach ($this->longs as $long) {
				if (strpos($long, $name) === 0)
					return true;
			}
		}
		return false;
	}
	
	/**
	 * Load the configuration for this option from a PHP array
	 * @api
	 * @param	array	$config	The configuration
	 * @return	Option			$this
	 */
	public function loadFromArray ($config) {
		foreach ($config as $key => $value) {
			switch ($key) {
				case "type":
					$type   = (is_array($value) and !empty($value)) ? $value[0] : $value;
					$regexp = (is_array($value) and sizeof($value) > 1 and $value[0] == "string") ? $value[1] : null;
					is_null ($regexp) ? $this->$type () : $this->$type ($regexp);
					break;
				case "shorts":
				case "akas":
				case "alias":
					foreach ((array)$value as $short)
						$this->aka ($short);
					break;
				case "longs":
					foreach ($value as $long)
						$this->long ($long);
					break;
				case "desc":
					$this->desc ($value);
					break;
				case "default":
					(is_array($value) and sizeof($value) > 1)
						? $this->default_ ($value[0], $value[1])
						: (is_array($value) ? $this->default_($value[0]) : $this->default_($value));
					break;
				case "follow":
				case "callback":
				case "obey":
				case "must":
					$rule      = (is_array($value) and !empty($value)) ? $value[0] : $value;
					$ruleError = (is_array($value) and sizeof($value) > 1) ? $value[1] : null;
					$callback  = $rule;
					if (strpos($rule, "PHP: ") === 0) {
						$callbackStr = '$callback = ' . substr ($rule, 5) . ";";
						eval ($callbackStr);
					} 
					is_null ($ruleError) ? $this->obey($callback) : $this->obey ($callback, $ruleError);
					break;
				case "group":
					$this->group ($value);
					break;
				case "require":
				case "req":
				case "required":
					$this->require_();
				default:
					break;
			}
		}
		return $this;
	}
	
}
