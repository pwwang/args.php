# Documentation for args.php
## Table of contents
* [What is args.php?](#markdown-header-what-is-argsphp)
* [Why args.php?](#markdown-header-why-argsphp)
* [Install](#markdown-header-install)
* [Settings for Args/Command instance](#markdown-header-program-usage)
	* [program usage](#markdown-header-program-usage)
	* [program description](#markdown-header-program-description)
	* [set help flags](#markdown-header-set-help-flags)
	* [help on "bald"](#markdown-header-help-on-bald)
	* [show warnings](#markdown-header-show-warnings)
	* [show types in help page](#markdown-header-show-types-in-help-page)
	* [allow tight mode](#markdown-header-allow-tight-mode)
* [Add an option](#markdown-header-add-an-option)
	* [set alias of an option](#markdown-header-set-alias-of-an-option)
	* [set long name of an option](#markdown-header-set-long-name-of-an-option)
	* [set description of an option](#markdown-header-set-description-of-an-option)
	* [set the default value of an option](#markdown-header-set-the-default-value-of-an-option)
	* [set an option as required](#markdown-header-set-an-option-as-required)
	* [set type of an option](#markdown-header-set-type-of-an-option)
	* [set callback of an option](#markdown-header-set-callback-of-an-option)
	* [group your options](#markdown-header-group-your-options)
* [Add a command](#markdown-header-add-a-command)
	* [set alias of a command](#markdown-header-set-alias-of-a-command)
	* [group your commands](#markdown-group-your-commands)
* [Chaining](#markdown-header-chaining)
* [Use configuration](#markdown-use-configuration)
	* [load configuration from a PHP array](#markdown-use-configuration)
	* [load configuration from a json file](#markdown-header-load-configuration-from-a-json-file)

<a name="markdown-header-what-is-argsphp"></a>
## What is args.php?
`args.php` is a library that handles the arguments from PHP command line.  
It cool and strong.

<a name="markdown-header-why-argsphp"></a>
## Why args.php?
There are some libraries doing this, but why `args.php`?

- It supports differents format of command line arguments.  
	`prog -a1 -b 2 --color red --dist=./out -vvv`
- It supports different types of options. 
	- array (`prog -a 1 2` or `prog -a 1 -a 2`)
	- bool (`prog -a 1` or `prog -a true`)
	- flag (`prog -useX`)
	- string (`prog -a somestring`)
	- string with Regexp constraint
- It can customize the usage/description of a program/command
- It supports commands (and commands of commands :)
- You can load the configuration from a config file
- You can group your options/commands
- ...

<a name="markdown-header-install"></a>
## Install
Install via [Composer](http://getcomposer.org/):

```json
"require": {
    "pwwang/args.php": "*"
}
```

or  
`composer require pwwang/args.php`  

Suggests:

- kevinlebrun/colors.php: Colorize PHP cli output,
- raveren/kint: Pretty `var_dump`/`print_r`

<a name="markdown-header-program-usage"></a>
## Settings for Args/Command instance
### program usage
Set the usage information in help page for the program/command.

```php
require_once "vendor/autoload.php";
$args = pw\Args\Args::factory(); 
// could be also initized using: $args = new pw\Args\Args();
$args->usage('script.php [options ...]');
```
You can use placeholders `{prog}`/`{command}` for the program/command name.  
You can also use a multiple-line string:

```php
$args->usage('script.php [usage1]
              script.php [usage2]'); 
```
`args.php` will trim the white space for each line.  
If you want some indents for some lines, you can use leading `_`:

```php
$args->usage('script.php [usage]
              __description for this usage.'); 
```
The same transformation is also applicable to [`Args::desc()`](#markdown-header-program-description), [`Option::desc()`](#markdown-header-set-description-of-an-option), `Command::desc()` and `Command::usage()`.

<a name="markdown-header-program-description"></a>
### program description
Set the description information in help page for the program/command.

```php
$args->desc('The description for this script/command
             __1. it does 1st thing,
             __2. it does 2nd thing.');
```
The string is similarly transformed as [`Args::usage()`](#markdown-header-program-usage).

<a name="markdown-header-set-help-flags"></a>
### set help flags
A program usually allows users to see the help page by, say, commonly `-h` and `--help`.

`args.php` allows developers to set differnt ones:

```php
$args->setHelpOpts("-?, --HELP"); // or
// $args->setHelpOpts(["-?, --HELP"]);
```
> NOTE: the first help flag must have at least one short name (starting with `-`, not `--`, names without leading `-` will be automatically added one.).

If the program has sub-commands, you don't need to set the flags for every commands, you just need to set the 2nd parameter as `true`:

```php
$args->setHelpOpts("?", true);
```
Then for the program and each command users can use `-?` to show the help page: `prog -?` or `prog command -?`.

<a name="markdown-header-help-on-bald"></a>
### help on "bald"
Show the help page if no arguments offered.  
This switch is **off** by default.

```php
// to switch on
$args->helpOnBald(); 
// to switch off (also for the child commands)
$args->helpOnBald(false);
// just to switch on for this command
$args->helpOnBald(true, false);
```
If the switch is off, some errors might be shown (i.e. missing required options), or the values of all options are taking the default values (if there is no required option).

<a name="markdown-header-show-warnings"></a>
### show warnings
Whether show the warnings or not. For example, if user offers two values for a non-array option: `prog -a 1 -a 2`, a warning will be triggered as previous value `1` is overriden.  
This switch is **on** by default.

```php
// switch on
$args->showWarnings();
// to switch off (also for the child commands)
$args->showWarnings(false);
// just to switch on for this command
$args->showWarnings(true, false); 
```

<a name="markdown-header-show-types-in-help-page"></a>
### show types in help page
Whether show types for options in the help page.  
This switch if **off** by default.

```php
// switch on
$args->showTypes();
// to switch off (also for the child commands)
$args->showTypes(false);
// just to switch on for this command
$args->showTypes(true, false); 
```

<a name="markdown-header-allow-tight-mode"></a>
### allow tight mode
Whether allow the tight mode (like `prog -a1 -b2`)
This switch if **off** by default.

```php
// switch on
$args->allowNoGap();
// to switch off (also for the child commands)
$args->allowNoGap(false);
// just to switch on for this command
$args->allowNoGap(true, false); 
```

<a name="markdown-header-add-an-option"></a>
## Add an option
To add an option:

```php
$option = $args->option("a"); // also allow name with length > 1
// $option = $args->option("abc"); // will accept value 1 in command line: prog -abc 1
```

<a name="markdown-header-set-alias-of-an-option"></a>
### set alias of an option
An option can have multiple alias:

```php
$option->alias("b");
$option->alias("cc"); // or 
// $option->aka("cc");   // or
// $option->short("cc");  
```
> NOTE: there is no much restriction on the alias, can even be an empty string `""`, but don't use alias with leading `-`, as it will be confused if you set alias `-d`, then when running `prog --d`, `args.php` regards it as a long name `d` instead of a short name `-d`.

<a name="markdown-header-set-long-name-of-an-option"></a>
### set long name of an option
An option can also have multiple long names:

```php
$option->long("long");  
```
The value will be receive when running: `prog --long 10` or `prog --long=10`.

<a name="markdown-header-set-description-of-an-option"></a>
### set description of an option
To explain what the option is for or what the option means, or anything you like about the option:

```php
$option->desc ("The input file.");
```
The string will be similarly transformed as [`Args::usage`](#markdown-header-program-usage).

<a name="markdown-header-set-the-default-value-of-an-option"></a>
### set the default value of an option
```php
$option->default (__DIR__ . "/abc.txt");
// to set the default value displayed in help page
// $option->default (__DIR__ . "/abc.txt", "./abc.txt");
```
If the 2nd parameter is not set, the default value displayed in help page will be json-encoded of the default value. For example, `true` => `"true"`.

<a name="markdown-header-set-an-option-as-required"></a>
### set an option as required
```php
$option->require (); // or
// $option->req(); // or
// $option->required();
```
> NOTE: if an option is set as required, its default value will be ignored.

<a name="markdown-header-set-type-of-an-option"></a>
### set type of an option
`args.php` supports `bool`, `array`, `flag`, `incr`, `string`, `string` with regexp constraint.

```php
$option->bool(); // or $option->boolean();
$option->array();
$option->flag();
$option->incr(3); // or $option->increment(3); // allow -v, -vv -vvv, but not -vvvv.
$option->string();
$option->string("/^\d+$/"); // only integer allowed
```
> NOTE: the difference between `bool` and `flag`:   
> You cannot set default value for flag option, if a flag option is offered, the value is true, otherwise false. But you can do that for a bool option, for example: `$option->default(true);`. If the option is not offered, the value will be true. It is false unless user passes a false value to the option: `-a false`.  
> 
> Users cannot either set a value for a flag option: `-a false`, `false` will be identified as unknown option/value.
>
> True values for bool option: `1,true,T,True,TRUE,Yes,YES,Y`
> False values for bool option: `0,false,F,False,FALSE,No,NO,N`

<a name="markdown-header-set-callback-of-an-option"></a>
### set callback of an option
Option callback is a powerful thing that can change the value of an option or validate the value of an option.

To validate the value:

```php
// alias: follow, must, obey
$option->callback (function($o) {
	if (!preg_match("/^\d+$/", $o->getValue()))
		return false;
}, "{option} should be an integer.");
```

To change the value of an option:

```php
$option->callback (function($o) {
	$val = $o->getValue();
	$o->setValue (str_replace("apple", "orange", $val));
});
```

<a name="markdown-header-group-your-options"></a>
### group your options
You can assign options to different groups, which will be shown in the help page.  

```php
$option1->group("Required options");
$option2->group("Optional options");
```

Don't forget that we have the help option, which belongs to group `Options`. You can assign a different group to it: `$args->helpOptGroup("Optional options")`.

You can also decide the order of the groups showing in the help page:

```php
$args->optGroupOrder ('Required options, Optional options'); // or
$args->optGroupOrder (['Required options, Optional options']);
```

> NOTE: When set the group order, `args.php` will NOT check the existence of the group names. Because we would allow you to set this before an option assigned to a group. So you have to check the group names yourself.

<a name="markdown-header-add-a-command"></a>
## Add a command
It's simple to add a command:

```php
$command = $args->command('command');
```
> NOTE: Once you add a command, the help command (`prog help`) is activated.
> You can customize your own help command by using: `$args->setHelpCmds("help, helpinfo");` or `$args->setHelpCmds(["help", "helpinfo"]);`

The class `pw\Args\Commnad` is extended from `pw\Args\Args`, so those settings are also suitable for `pw\Args\Commnad`. Besides, it has its own settings/methods.

<a name="markdown-header-set-alias-of-a-command"></a>
### Set alias of a command:

```php
$command->aka ('cmd'); // or
$command->alias ('cmd');
```

<a name="markdown-group-your-commands"></a>
### Group your commands:

```php
$command1 = $args->command("command1")->group ("Group1 commands");
$command2 = $args->command("command2")->group ("Group2 commands");
```

Similarly, you may also need to assign a group for the help command:

```php
$args->helpCmdGroup ("Group1 commands");
```

And you could also assign the order of the groups:

```php
$args->cmdGroupOrder ("Group1 commands, Group2 commands"); // or
$args->cmdGroupOrder (["Group1 commands", "Group2 commands"]);
```

> NOTE: similarly as [option groups](#markdown-header-group-your-options), we won't check the existence of the group names.

<a name="markdown-header-chaining"></a>
## Chaining

For convenience, we allow the chaining operation. But please pay attention to the context of `$this`:

```php
$args = pw\Args\Args::factory ()                 // instanceof Args
	-> helpOnBald()                              // instanceof Args
	-> usage ("{prog} <command> [options ...]")  // instanceof Args
	-> option ("a")                              // instanceof Option
	-> desc ("Option a")                         // instanceof Option
	-> end ()                                    // instanceof Args, so that I can add another option
	-> option ("b")                              // instanceof Option 
	-> end ()                                    // instanceof Args, so that I can add a command
	-> command ("command1")                      // instanceof Command
	-> option ("a")                              // instanceof Option
	-> end ()                                    // instanceof Command
	-> end ()                                    // instanceof Args
	-> done ();                                  // instanceof Args
``` 

<a name="markdown-use-configuration"></a>
## Use configuration

### load configuration from a PHP array
```php
$args = pw\Args\Args::factory()
	-> loadFromArray ([
		"helpOptGroup" => "Group1 options",
		"options" => [
			"a" => [
				"desc" => "Option a",
				"group" => "Group1 options"
			],
			"b" => [
				"desc" => "Option b",
				"group" => "Group1 options"
			],
			"c" => [
				"desc" => "Option c",
				"group" => "Group2 options"
			],
			"d" => [
				"desc" => "Option d",
				"group" => "Group2 options"
			]
		]
	]);
// you can do something more or $args, i.e. set the type of option -d 
// $args->getOption("d")->array();
$args-> done ();
```

<a name="markdown-header-load-configuration-from-a-json-file"></a>
### load configuration from a json file
If you don't want the configure take too much space in your script file (say `script.php`, you can define them in JSON format in a separate file. (`script.args.json` by default)

```php
$args = pw\Args\Args::factory()->done ();
// or you can define in a different file
$args = pw\Args\Args::factory($configfile)->done ();
//// or 
$args = new pw\Args\Args($configfile);
$args->done();
```

Full schema of the configuration file:

```json
{
	"desc"               : "[string] The description of the program/script",
	"usage"              : "[string] The usage of the program/script (you may use {prog} as the program name placeholder)",
	"helpCmds"           : "[string|array] The commands that will show the help page. (default: help, could use array or comma-separated string)",
	"helpCmdsGlobal"     : "[bool] true (default) to apply helpCmds to child commands, false to use default for them.",
	"helpOpts"           : "[string|array] The options that will show the help page. (default: '-h, --help', could use array or comma-separated string)",
	"helpOptsGlobal"     : "[bool] true (default) to apply helpOpts to child commands, false to use default for them.",
	"helpOnBald"         : "[bool] true to show the help page if no arguments offered, false else (default).",
	"helpOnBaldGlobal"   : "[bool] true (default) to apply helpOnBald to child commands, false to use default for them.",
	"showWarnings"       : "[bool] true (default) to show warnings, false else.",
	"showWarningsGlobal" : "[bool] true (default) to apply showWarnings to child commands, false to use default for them.",
	"showTypes"          : "[bool] true to show types for options in the help page, false else (default).",
	"showTypesGlobal"    : "[bool] true (default) to apply showTypes to child commands, false to use default for them.",
	"allowNoGap"         : "[bool] true to allow the tight mode (prog -a1 -b2), false else (default).",
	"allowNoGapGlobal"   : "[bool] true (default) to apply allowNoGap to child commands, false to use default for them.",
	"helpOptGroup"       : "[string] the group that the help option belongs to.",
	"helpCmdGroup"       : "[string] the group that the help command belongs to.",
	"optGroupOrder"      : "[string|array] the order of option groups.",
	"cmdGroupOrder"      : "[string|array] the order of command groups.",
	
	"commands"           : {
		"cmd-name1" : {
			"alias" : "[array] the alias of the command",
			"group" : "[string] the group of the command.",
			// Other key-values are similar as top-level ones, i.e. desc, usage, options, ...
		},
		"cmd-name2" : {
			"alias" : "[array] the alias of the command",
			"group" : "[string] the group of the command.",
			// Other key-values are similar as top-level ones, i.e. desc, usage, options, ...
		}
	}

	"options" : {
		"a" : {
			"type": ["string", "/^\d+$/"], // string with regexp constraint
			"alias": ["aopt", "aoption"],  // shorts, akas
			"longs": ["aaaoption"],
			"desc": "The option a",
			"require": true,               // req, required
			"group": "Group1 options"
		},
		"v" : {
			"type": ["incr", 3],           // allows -v, -vv, -vvv
			"longs": ["verbose"],          // also allows --verbose=3
			"default": 1,                  // or [1, "-v"], show -v instead of 1 in help page
		},
		"file1" : {
			"type": "string",
			"obey": ["file_exists", "file does not exist for option {option}"]
			// or must, callback, follow
		},
		"file2" : {
			"callback": "PHP: function($opt) {$opt->setValue(fopen($opt->getValue(), "r"));}"
		}
	}
		
}
```